﻿using Polly;
using SQLite;
using System;
using System.Threading.Tasks;
using WeatherApp.Shared.Repository.Interfaces;

namespace WeatherApp.Shared.Repository.Implementations
{
    public class RepositoryBase
    {

        protected IDBConnection _dbConnection;

        public RepositoryBase(IDBConnection lazyDBConnection)
        {
            _dbConnection = lazyDBConnection;
        }

        protected static Task<T> AttemptAndRetry<T>(Func<Task<T>> action, int numRetries = 3)
        {
            return Policy.Handle<SQLiteException>().WaitAndRetryAsync(numRetries, pollyRetryAttempt).ExecuteAsync(action);

            static TimeSpan pollyRetryAttempt(int attemptNumber) => TimeSpan.FromMilliseconds(Math.Pow(2, attemptNumber));
        }

        protected static Task AttemptAndRetry(Func<Task> action, int numRetries = 3)
        {
            return Policy.Handle<SQLiteException>().WaitAndRetryAsync(numRetries, pollyRetryAttempt).ExecuteAsync(action);

            static TimeSpan pollyRetryAttempt(int attemptNumber) => TimeSpan.FromMilliseconds(Math.Pow(2, attemptNumber));
        }
    }
}
