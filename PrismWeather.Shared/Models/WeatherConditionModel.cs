﻿using Newtonsoft.Json;

namespace WeatherApp.Shared.Models
{
    public class WeatherConditionModel
    {
        private string _icon;

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("icon")]
        public string Icon
        {
            get { return $"http:{_icon}"; }
            set { _icon = value; }
        }

        [JsonProperty("code")]
        public int Code { get; set; }
    }
}