﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Data
{
    public class WeatherContextService : IWeatherContextService
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IConfiguration _configuration;
        private string authKey;
        private string baseAPI;

        public WeatherContextService(IHttpClientFactory clientFactory,
            IConfiguration configuration)
        {
            _clientFactory = clientFactory;
            _configuration = configuration;

            authKey = _configuration.GetValue<string>("ApiKey"); 
            baseAPI =_configuration.GetValue<string>("WeatherBaseApi");
        }

        public async Task<DbWeather> LoadWeatherBySearch(string q)
        {
            DbWeather result = null;

            var request = new HttpRequestMessage(HttpMethod.Get,
                $"{baseAPI}current.json?key={authKey}&q={q}");

            var client = _clientFactory.CreateClient();

            using (HttpResponseMessage response = await client.SendAsync(request))
            {
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<DbWeather>();
                    return result;
                }
            }
            return result;
        }

        public async Task<IEnumerable<DbSearchedLocation>> LoadLocationsBySearch(string q)
        {
            IEnumerable<DbSearchedLocation> result = null;

            var request = new HttpRequestMessage(HttpMethod.Get,
                $"{baseAPI}search.json?key={authKey}&q={q}");

            var client = _clientFactory.CreateClient();

            using (HttpResponseMessage response = await client.SendAsync(request))
            {
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<IEnumerable<DbSearchedLocation>>();
                    return result;
                }
            }
            return result;
        }

        public async Task<DbWeather> LoadForecastByLocation(string q)
        {
            DbWeather result = null;

            var request = new HttpRequestMessage(HttpMethod.Get,
                $"{baseAPI}forecast.json?key={authKey}&q={q}&days=3");

            var client = _clientFactory.CreateClient();

            using (HttpResponseMessage response = await client.SendAsync(request))
            {
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<DbWeather>();
                    return result;
                }
            }
            return result;
        }
    }
}
