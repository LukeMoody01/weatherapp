﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherApp.Shared.Authentication
{
    public class AuthenticateManager : IAuthenticateManager
    {
        public string Token { get; private set; }

        public AuthenticateManager()
        {
        }

        public void ForceSetToken(string token)
            => Token = token;

    }
}
