﻿using CoreWeatherAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace CoreWeatherAPI.Data
{
    public class WeatherDbContext : DbContext
    {
        public WeatherDbContext(DbContextOptions<WeatherDbContext> options)
            : base(options)
        {

        }
        public DbSet<DbUserModel> Users { get; set; }
        public DbSet<DbSearchedLocation> UserFavouritedLocations { get; set; }
        public DbSet<DbForgottenPassword> ForgotPasswords { get; set; }
    }
}
