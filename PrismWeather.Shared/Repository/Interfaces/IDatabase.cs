﻿using System.Threading.Tasks;

namespace WeatherApp.Shared.Repository.Interfaces
{
    public interface IDatabase
    {
        void SetOldDBPath(string oldDBPath);
        Task InitializeDatabase();
        void DeleteOldDatabase();
        int GetOldDatabaseVersion();
        int GetCurrentDatabaseVersion();
        string GetOldDBPath();
        Task DeleteTables();
    }
}