﻿using Microsoft.Extensions.DependencyInjection;
using Shiny;
using WeatherApp.Infra;

namespace PrismWeatherApp.Droid
{
    public class AndroidStartup : WeatherAppStartup
    {
        public override void ConfigureServices(IServiceCollection services, IPlatform platform)
        {
            base.ConfigureServices(services, platform);
        }
    }
}