﻿using System.Collections.Generic;
using WeatherApp.Shared.Models;

namespace WeatherApp.Shared.Authentication
{
    public class CurrentUser : ICurrentUser
    {
        public UserModel person { get; set; }
        public List<SearchedLocationModel> favourites { get; set; }
    }
}
