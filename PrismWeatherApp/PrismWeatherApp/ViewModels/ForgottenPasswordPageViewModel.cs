﻿using Prism.Commands;
using Prism.Navigation;
using System.Threading.Tasks;
using System.Windows.Input;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.Services.Interfaces;

namespace PrismWeatherApp.ViewModels
{
    public class ForgottenPasswordPageViewModel : ViewModelBase
    {
        #region Fields
        private readonly IAppUserService _appUserService;
        #endregion

        #region Properties
        public string EmailAddress { get; set; }
        #endregion

        #region Commands
        public ICommand LoginCommand { get; set; }
        public ICommand ResetPasswordCommand { get; set; }
        #endregion

        #region Ctor
        public ForgottenPasswordPageViewModel(IBaseService baseService,
            IAppUserService appUserService) : base(baseService)
        {
            _appUserService = appUserService;
            LoginCommand = new DelegateCommand(async () => await NaviagateToLogin());
            ResetPasswordCommand = new DelegateCommand(async () => await ResetPassword());
        }
        #endregion

        #region Methods
        private async Task ResetPassword()
        {
            IsBusy = true;

            if (string.IsNullOrWhiteSpace(EmailAddress))
            {
                IsBusy = false;
                await ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORHEADER, AppConstants.ERRORNOEMAILFOUND);
                return;
            }

            var existingUser = await _appUserService.GetExistingUser(EmailAddress);

            if (existingUser == null)
            {
                IsBusy = false;
                await ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORHEADER, AppConstants.ERRORNOEMAILFOUND);
                return;
            }

            await _appUserService.ResetPassword(EmailAddress);

            IsBusy = false;
            await ShowPopup(AppConstants.SUCCESSANIMATION, AppConstants.SUCCESSEMAILHEADER, AppConstants.SUCCESSEMAILSENT);
        }

        private Task NaviagateToLogin()
        {
            return _navigationService.NavigateAsync("/LoginPage");
        }
        #endregion
    }
}
