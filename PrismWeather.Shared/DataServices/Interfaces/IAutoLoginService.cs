﻿using System.Threading.Tasks;

namespace WeatherApp.Shared.Services.Interfaces
{
    public interface IAutoLoginService
    {
        Task GetUserData(string userId);
    }
}