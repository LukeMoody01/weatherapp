﻿namespace WeatherApp.Shared.BarrackShared
{
    public class AppConstants
    {
        #region Popups

        //Popup Animations
        public const string SUCCESSANIMATION = "success.json";
        public const string FAVOURITEANIMATION = "favourite.json";
        public const string ERRORANIMATION = "error.json";

        //Popup Headers
        public const string SUCCESSHEADER = "Success";
        public const string FAVOURITEHEADER = "Favourited";
        public const string SUCCESSEMAILHEADER = "Email Sent";

        public const string ERRORHEADER = "Error";
        public const string ERRORSERACHHEADER = "Not Vaild Search";

        //Popup Body
        public const string SUCCESSREGISTER = "Your Account has been created, go to the Login Page to Log In!";
        public const string SUCCESSEMAILSENT = "Check your email for a new password!";
        public const string SUCCESSLOCATIONFAVOURITE = "was set as a favourite!";
        public const string SUCCESSREMOVELOCATIONFAVOURITE = "was removed as a favourite!";

        public const string ERRORREGISTER = "Your Account was not created, Please try again!";
        public const string ERROREMAILTAKEN = "The email you entered already exists, Please go to the Login Page to Log In!";
        public const string ERRORNOEMAILFOUND = "The email you entered does not exists, Please go to the Register Page to Register an account!";
        public const string ERRORINCORRECTINFO = "The information you entered is not correct, Please try again!";
        public const string ERRORINVALIDINFO = "The information you was invalid, Please try again!";
        public const string ERRORINTERNAL = "There was an error. (Internal Error)";
        public const string ERRORLOCATIONFAVOURITE = "was not set as a favourite. (Internal Error)";
        public const string ERRORREMOVELOCATIONFAVOURITE = "was not removed as a favourite. (Internal Error)";
        public const string ERRORSEARCHNOTVALID = "The location you entered is not valid, try the city name";
        public const string ERRORNOACCESSTOLOCATION = "This app requires access to your location, please allow access and try again!";

        #endregion

        #region DB

        public const string DATABASE_NAME = "WeatherDB.db3";
        public const string ENCRYPTED_DATABASE_NAME = "EncWeatherDB.db3";
        public const string STANDARD_DATABASE_NAME = "SWeatherDB.db3";

        #endregion

    }
}
