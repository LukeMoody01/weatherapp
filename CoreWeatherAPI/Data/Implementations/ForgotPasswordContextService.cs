﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Data
{
    public class ForgotPasswordContextService : IForgotPasswordContextService
    {
        private readonly WeatherDbContext _dbContext;

        public ForgotPasswordContextService(WeatherDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public DbForgottenPassword GetForgotPasswordRequest(string reqId)
        {
            DbForgottenPassword validRequest = _dbContext.ForgotPasswords.FirstOrDefault(x => x.Id == reqId 
                                                                        && x.ExpireyDate > DateTime.Now
                                                                        && x.IsFulfilled == false);
            return validRequest;
        }

        public async Task<bool> UpdateRequest(DbForgottenPassword forgotPassword)
        {
            _dbContext.Update(forgotPassword);
            await _dbContext.SaveChangesAsync();
            return true;
        }
    }
}
