﻿using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Data
{
    public class UserContextService : IUserContextService
    {
        private WeatherDbContext _context;
        private readonly IConfiguration _configuration;
        private string thisAPI;

        public UserContextService(WeatherDbContext context, 
            IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            thisAPI = _configuration.GetValue<string>("ApiHost");
        }

        public async Task<bool> CreateUser(DbUserModel user)
        {
            {
                var savedPasswordHash = CreateHashedPassword(user.Password);
                user.Password = savedPasswordHash;

                await _context.Users.AddAsync(user);
                await _context.SaveChangesAsync();
                return true;
            }
        }

        private string CreateHashedPassword(string password)
        {
            // Storing a secure password

            // STEP 1 Create the salt value
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

            // Step 2 Create the Rfc2898DeriveBytes and get the hash value
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);

            // Step 3 Combine the salt and password bytes for later use:
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            string savedPasswordHash = Convert.ToBase64String(hashBytes);
            return savedPasswordHash;
        }

        public async Task<bool> SendResetEmail(string userEmail)
        {
            var user = _context.Users.FirstOrDefault(f => f.EmailAddress == userEmail);
            if (user == null)
                return false;

            var resetRequest = new DbForgottenPassword
            {
                Id = Guid.NewGuid().ToString(),
                UserId = user.Id,
                CreatedDate = DateTime.Now,
                ExpireyDate = DateTime.Now.AddMinutes(30),
                IsFulfilled = false
            };

            await _context.ForgotPasswords.AddAsync(resetRequest);

            await _context.SaveChangesAsync();

            var link = $"{thisAPI}ForgottenPassword/ForgotPassword/{resetRequest.Id}";

            var apiKey = _configuration.GetValue<string>("MoodyMail");
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("lukemoodyait@gmail.com", "Password Client");
            var subject = "Reset Password";
            var to = new EmailAddress(userEmail, "User");
            var plainTextContent = $"Please visit this link to reset your password - {link}";
            var htmlContent = $"Please visit this link to reset your password - {link}\n This expires in 30 minutes.";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> ResetPassword(string passWord, string userId)
        {
            var user = GetUserById(userId);
            var hashPassword = CreateHashedPassword(passWord);
            user.Password = hashPassword;
            _context.Update(user);
            await _context.SaveChangesAsync();

            return true;
        }

        public DbUserModel GetUserById(string userID)
        {
            return _context.Users.First(u => u.Id == userID);
        }

        public DbUserModel Login(string email)
        {
            var user = _context.Users.FirstOrDefault(u => u.EmailAddress == email);
            return user;
        }

        public DbUserModel GetUser(string email)
        {
            var user = _context.Users.FirstOrDefault(u => u.EmailAddress == email);
            return user;
        }

        public async Task<bool> CreateFavourite(DbSearchedLocation favourite)
        {
            await _context.UserFavouritedLocations.AddAsync(favourite);
            await _context.SaveChangesAsync();
            return true;
        }

        public IEnumerable<DbSearchedLocation> GetFavourites(string userId)
        {
            IEnumerable<DbSearchedLocation> loactions = _context.UserFavouritedLocations.Where(u => u.UserFavourited == userId);
            return loactions;
        }

        public async Task<bool> DeleteFavourite(DbSearchedLocation favourite)
        {
            var location = _context.UserFavouritedLocations.FirstOrDefault(f => f.UserFavourited == favourite.UserFavourited && f.Id == favourite.Id);
            _context.UserFavouritedLocations.Remove(location);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
