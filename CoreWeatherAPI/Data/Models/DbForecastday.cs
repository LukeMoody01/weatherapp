﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Data
{
    public class DbForecastday
    {
        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("date_epoch")]
        public int DateEpoch { get; set; }

        [JsonProperty("day")]
        public DbDay Day { get; set; }

        [JsonProperty("astro")]
        public DbAstro Astro { get; set; }

        [JsonProperty("hour")]
        public List<DbHour> Hour { get; set; }
    }
}
