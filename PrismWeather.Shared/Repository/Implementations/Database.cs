﻿using SQLite;
using System;
using System.IO;
using System.Threading.Tasks;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Repository.Interfaces;

namespace WeatherApp.Shared.Repository.Implementations
{
    public class Database : RepositoryBase, IDatabase
    {
        const int _databaseVersion = 1;
        string _oldDBPath;
        public Database(IDBConnection lazyDBConnection) : base(lazyDBConnection) { }

        public void DeleteOldDatabase()
        {
            if (_databaseVersion >= 1)
            {
                var previousDatabaseVersion = GetOldDatabaseVersion();

                if (previousDatabaseVersion < _databaseVersion)
                {
                    if (File.Exists(_oldDBPath))
                        File.Delete(_oldDBPath);
                }
            }
        }

        public async Task DeleteTables()
        {
            var databaseConnection = _dbConnection.GetAsyncConnection();

            await AttemptAndRetry(async () =>
            {
                await databaseConnection.DropTableAsync<UserModel>();
                await databaseConnection.DropTableAsync<SearchedLocationModel>();

            }).ConfigureAwait(false);

        }

        public int GetCurrentDatabaseVersion()
        {
            return _databaseVersion;
        }

        public int GetOldDatabaseVersion()
        {
            try
            {
                if (File.Exists(_oldDBPath))
                {
                    using (var con = new SQLiteConnection(_oldDBPath))
                    {
                        return con.ExecuteScalar<int>("PRAGMA user_version");
                    }
                }
            }
            catch (Exception)
            {

            }

            return _databaseVersion;
        }

        public string GetOldDBPath()
        {
            return _oldDBPath;
        }

        public async Task InitializeDatabase()
        {
            int currentDbVersion = await GetDatabaseVersion();

            if (currentDbVersion < _databaseVersion)
                await UpgradeDatabase();
            else
                await CreateTables();
        }

        private async Task CreateTables()
        {
            var con = _dbConnection.GetAsyncConnection();

            await AttemptAndRetry(async () =>
            {
                await con.CreateTableAsync<UserModel>();
                await con.CreateTableAsync<SearchedLocationModel>();

                await con.ExecuteAsync("PRAGMA foreign_keys = ON");
            });
        }

        public void SetOldDBPath(string dbPath)
        {
            _oldDBPath = dbPath;
        }

        private async Task<int> GetDatabaseVersion()
        {
            try
            {
                var databaseConnection = _dbConnection.GetAsyncConnection();
                return await AttemptAndRetry(() => databaseConnection.ExecuteScalarAsync<int>("PRAGMA user_version")).ConfigureAwait(false);
            }
            catch
            {
                _dbConnection?.DeleteDatabaseFile();
            }

            return 0;
        }

        private async Task<int> SetDatabaseToVersion(int version)
        {
            var databaseConnection = _dbConnection.GetAsyncConnection();

            return await AttemptAndRetry(() => databaseConnection.ExecuteAsync("PRAGMA user_version = " + version)).ConfigureAwait(false);
        }
        private async Task UpgradeDatabase()
        {
            //the first time ever we get this value after the database creation
            //this should be equals 0
            int currentDbVersion = await GetDatabaseVersion();

            if (currentDbVersion < _databaseVersion)
            {
                await DeleteTables();
                await CreateTables();

                await SetDatabaseToVersion(_databaseVersion);
            }
        }
    }
}
