﻿using System;
using System.IO;
using WeatherApp.Utility;
using Xamarin.Forms;

namespace WeatherApp.Utility
{
    public class LocalDBUtility : ILocalDBUtility
    {
        public LocalDBUtility()
        {

        }

        public string GetDBPath(string filename)
        {
            if (Device.RuntimePlatform == Device.iOS)
            {
                string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                string libFolder = Path.Combine(docFolder, "..", "Library", "Databases");

                if (!Directory.Exists(libFolder))
                {
                    Directory.CreateDirectory(libFolder);
                }

                return Path.Combine(libFolder, filename);
            }
            else if (Device.RuntimePlatform == Device.Android)
            {
                string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                return Path.Combine(path, filename);
            }
            else
            {
                return null;
            }
        }
    }
}