﻿using System.Collections.Generic;
using WeatherApp.Shared.Models;

namespace WeatherApp.Shared.Authentication
{
    public interface ICurrentUser
    {
        UserModel person { get; set; }
        List<SearchedLocationModel> favourites { get; set; }
    }
}