﻿using Newtonsoft.Json;
using Polly;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.Models;

namespace WeatherApp.Shared.GatewayAccess.Base
{
    public abstract class GatewayServiceBase
    {
        #region Fields

        private static HttpClient _httpClient;

        #endregion

        #region Properties

        protected IAppSettingsProvider _appSettingsProvider { get; }

        public string BaseAPI { get; }

        #endregion

        public GatewayServiceBase(IAppSettingsProvider appSettingsProvider)
        {
            _appSettingsProvider = appSettingsProvider;
            BaseAPI = appSettingsProvider.GetRestBaseApi();
        }

        #region Methods

        public HttpClient GetClient()
        {
            _httpClient = (_httpClient ??= new HttpClient(new AuthenticatedHttpClient()));

            return _httpClient;
        }

        protected StringContent CastToStringContent<T>(T item)
        {
            var jsonString = JsonConvert.SerializeObject(item);
            var jsonContent = new StringContent(jsonString, Encoding.UTF8, "application/json");
            return jsonContent;
        }

        protected async Task<T> PostRemoteAsync<T>(string endPoint, HttpContent httpContent)
        {
            string errorResult = "";
            HttpResponseMessage jsonResponse = new HttpResponseMessage();

            try
            {
                jsonResponse = await Policy
                        .Handle<Exception>()
                        .WaitAndRetryAsync
                        (
                          retryCount: 3,
                          sleepDurationProvider: retryAttempt => TimeSpan.FromSeconds(3)
                        )
                       .ExecuteAsync(async () =>
                       {
                           var httpClient = GetClient();
                           var result = await httpClient.PostAsync(endPoint, httpContent).ConfigureAwait(false);
                           return result;
                       });

                if (jsonResponse.IsSuccessStatusCode)
                {
                    var jsonResult = await jsonResponse.Content.ReadAsStringAsync().ConfigureAwait(false);

                    if (!string.IsNullOrWhiteSpace(jsonResult))
                    {
                        var deserializedObject = JsonConvert.DeserializeObject<T>(jsonResult);
                        return deserializedObject;
                    }
                    else
                    {
                        return default(T);
                    }
                }
                else
                {
                    errorResult = await jsonResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                    throw new Exception();
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }

            throw new ArgumentException(string.Format("Not successful in getting results from this api {0}. The response error was {1}.", endPoint, errorResult));
        }

        protected async Task<T> PutRemoteAsync<T>(string endPoint, HttpContent httpContent)
        {
            string errorResult = "";
            HttpResponseMessage jsonResponse = new HttpResponseMessage();

            try
            {
                jsonResponse = await Policy
                        .Handle<Exception>()
                        .WaitAndRetryAsync
                        (
                          retryCount: 3,
                          sleepDurationProvider: retryAttempt => TimeSpan.FromSeconds(3)
                        )
                       .ExecuteAsync(async () =>
                       {
                           var httpClient = GetClient();
                           var result = await httpClient.PutAsync(endPoint, httpContent).ConfigureAwait(false);
                           return result;
                       });

                if (jsonResponse.IsSuccessStatusCode)
                {
                    var jsonResult = await jsonResponse.Content.ReadAsStringAsync().ConfigureAwait(false);

                    if (!string.IsNullOrWhiteSpace(jsonResult))
                    {
                        var deserializedObject = JsonConvert.DeserializeObject<T>(jsonResult);
                        return deserializedObject;
                    }
                    else
                    {
                        return default(T);
                    }
                }
                else
                {
                    errorResult = await jsonResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                    throw new Exception();
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }

            throw new ArgumentException(string.Format("Not successful in getting results from this api {0}. The response error was {1}.", endPoint, errorResult));
        }

        protected async Task<T> GetRemoteAsync<T>(string endPoint)
        {
            string errorResult = "";
            HttpResponseMessage jsonResponse = new HttpResponseMessage();

            try
            {
                jsonResponse = await Policy
                        .Handle<Exception>()
                        .WaitAndRetryAsync
                        (
                          retryCount: 3,
                          sleepDurationProvider: retryAttempt => TimeSpan.FromSeconds(3)
                        )
                       .ExecuteAsync(async () =>
                       {
                           var httpClient = GetClient();
                           var result = await httpClient.GetAsync(endPoint).ConfigureAwait(false);
                           return result;
                       });

                if (jsonResponse.IsSuccessStatusCode)
                {
                    var jsonResult = await jsonResponse.Content.ReadAsStringAsync().ConfigureAwait(false);

                    if (!string.IsNullOrWhiteSpace(jsonResult))
                    {
                        var deserializedObject = JsonConvert.DeserializeObject<T>(jsonResult);
                        return deserializedObject;
                    }
                    else
                    {
                        return default(T);
                    }
                }
                else
                {
                    errorResult = await jsonResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                    throw new Exception();
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }

            throw new ArgumentException(string.Format("Not successful in getting results from this api {0}. The response error was {1}.", endPoint, errorResult));
        }

        protected async Task<T> DeleteRemoteAsync<T>(string endPoint, StringContent content)
        {
            string errorResult = "";
            HttpResponseMessage jsonResponse = new HttpResponseMessage();
            var request = new HttpRequestMessage(HttpMethod.Delete, endPoint);
            request.Content = content;

            try
            {
                jsonResponse = await Policy
                        .Handle<Exception>()
                        .WaitAndRetryAsync
                        (
                          retryCount: 3,
                          sleepDurationProvider: retryAttempt => TimeSpan.FromSeconds(3)
                        )
                       .ExecuteAsync(async () =>
                       {
                           var httpClient = GetClient();
                           var result = await httpClient.SendAsync(request).ConfigureAwait(false);
                           return result;
                       });

                if (jsonResponse.IsSuccessStatusCode)
                {
                    var jsonResult = await jsonResponse.Content.ReadAsStringAsync().ConfigureAwait(false);

                    if (!string.IsNullOrWhiteSpace(jsonResult))
                    {
                        var deserializedObject = JsonConvert.DeserializeObject<T>(jsonResult);
                        return deserializedObject;
                    }
                    else
                    {
                        return default(T);
                    }
                }
                else
                {
                    errorResult = await jsonResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                    throw new Exception();
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }

            throw new ArgumentException(string.Format("Not successful in getting results from this api {0}. The response error was {1}.", endPoint, errorResult));
        }

        #endregion
    }
}
