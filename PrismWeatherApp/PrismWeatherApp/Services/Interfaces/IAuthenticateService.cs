﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WeatherApp.Shared.Models;

namespace WeatherApp.Services
{
    public interface IAuthenticateService
    {
        Task Logout();
        Task<bool> Login(LoginRequestModel loginRequest);
        Task<bool> IsUserLoggedIn();
    }
}
