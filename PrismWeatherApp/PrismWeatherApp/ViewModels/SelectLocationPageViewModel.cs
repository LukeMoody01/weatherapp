﻿using Prism.Commands;
using Prism.Navigation;
using Prism.Navigation.TabbedPages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Services.Interfaces;

namespace PrismWeatherApp.ViewModels
{
    public class SelectLocationPageViewModel : ViewModelBase
    {
        #region Feilds
        private ObservableCollection<SearchedLocationModel> _searchedLocations;
        private SearchedLocationModel _selectedLocation;
        #endregion

        #region Properties
        public ObservableCollection<SearchedLocationModel> SearchedLocations
        {
            get { return _searchedLocations; }
            set
            {
                SetProperty(ref _searchedLocations, value);
                RaisePropertyChanged(nameof(SearchedLocations));
            }
        }

        public SearchedLocationModel SelectedLocation
        {
            get { return _selectedLocation; }
            set
            {
                SetProperty(ref _selectedLocation, value);
                SelectedWeatherLocationCommand.Execute(_selectedLocation);
            }
        }
        #endregion

        #region Commands
        public ICommand SelectedWeatherLocationCommand { get; set; }
        #endregion

        #region Ctor
        public SelectLocationPageViewModel(IBaseService baseService) : base(baseService)
        {
            SelectedWeatherLocationCommand = new DelegateCommand<SearchedLocationModel>(NavigateToSearchedLocation);
        }
        #endregion

        #region Methods
        private async void NavigateToSearchedLocation(SearchedLocationModel searchedLocation)
        {
            if (SelectedLocation == null)
                return;

            if (!CanClick(1000))
                return;

            IsBusy = true;

            var navP = new NavigationParameters();
            navP.Add("SelectedLocaton", SelectedLocation);
            navP.Add("LocationData", SelectedLocation.Name);

            await _navigationService.SelectTabAsync("WeatherPage", navP);
            IsBusy = false;
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            IsBusy = true;
            SelectedLocation = null;

            var locations = parameters.GetValue<List<SearchedLocationModel>>("SearchedLocations");

            if (locations == null)
            {
                IsBusy = false;
                return;
            }
            SearchedLocations = new ObservableCollection<SearchedLocationModel>(locations);
            IsBusy = false;
        }
        #endregion
    }
}
