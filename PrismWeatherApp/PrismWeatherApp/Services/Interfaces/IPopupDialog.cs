﻿using System.Threading.Tasks;

namespace WeatherApp.Services
{
    public interface IPopupDialog
    {
        Task ShowPopup(string popupAnimationType, string popupHeader, string popupBody);
    }
}
