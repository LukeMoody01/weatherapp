﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Shared.Models;

namespace WeatherApp.Shared.GatewayAccess.Interfaces
{
    public interface IAppUserGateway
    {
        Task<string> GetToken(LoginRequestModel loginRequest);
        Task<bool> PostCreateUser(UserModel user);
        Task<UserModel> Login(LoginRequestModel loginRequest);
        Task<bool> PostResetPassword(string email);
        Task<UserModel> GetUserForRegistration(string email);
    }
}