﻿using FluentAssertions;
using Moq;
using Moq.AutoMock;
using Prism.Navigation;
using PrismWeatherApp.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Shared.Authentication;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Services.Interfaces;
using Xunit;

namespace WeatherApp.Test.ViewModels
{
    public class WeatherPageViewModelTests
    {
        private WeatherPageViewModel Sut { get; }
        private AutoMocker Mocker { get; }
        public WeatherPageViewModelTests()
        {
            Mocker = new AutoMocker(Moq.MockBehavior.Default, Moq.DefaultValue.Mock);
            Sut = Mocker.CreateInstance<WeatherPageViewModel>();
        }

        [Fact]
        public async Task InitializeAsync_ShouldPopulateData_WhenLoaded()
        {
            //Arrange
            var weatherLocaton = "Sydney";
            var localTime = "2021/12/01";
            var tempC = 22;
            var windKph = 22;
            var areaPressure = 22;
            var humidity = 22;
            var cloud = 22;
            var weatherConditionText = "This is weather";

            var locationData = new WeatherModel
            { 
                Current = new CurrentWeatherModel
                {
                   WindKph = windKph,
                   TempC = tempC,
                   PressureMb = areaPressure,
                   Humidity = humidity,
                   Cloud = cloud,
                   Condition = new WeatherConditionModel 
                   { 
                       Text = weatherConditionText
                   }

                },
                Location = new WeatherLocationModel
                {
                    Name = weatherLocaton,
                    Localtime = localTime
                },
                Forecast = new ForecastModel 
                {
                    Forecastday = new List<ForecastDayModel>()
                }
            };

            Mocker.GetMock<IWeatherService>().Setup(x => x.GetForecast(It.IsAny<string>()))
                                             .ReturnsAsync(locationData);

            var parameters = new NavigationParameters();
            parameters.Add("LocationData", "Sydney");

            //Act
            await Sut.InitializeAsync(parameters);

            //Assert
            Sut.WeatherLocation.Should().Be(weatherLocaton);
            Sut.LocalTime.Should().Be(localTime);
            Sut.TempC.Should().Be($"{tempC}");
            Sut.WindKPH.Should().Be($"{windKph} kph");
            Sut.AreaPressure.Should().Be($"{areaPressure} hpa");
            Sut.Humidity.Should().Be($"{humidity}%");
            Sut.Cloudiness.Should().Be($"{cloud}%");
            Sut.WeatherConditionText.Should().Be(weatherConditionText);
        }

        [Fact]
        public async Task UpdateFavourite_ShouldNotBeFavourite_WhenClickngUnSaveFavourite()
        {
            //Arrange
            Sut.WeatherLocation = "Sydney";

            Mocker.GetMock<IWeatherService>().Setup(x => x.DeleteFavourite(It.IsAny<SearchedLocationModel>()))
                                             .ReturnsAsync(true);

            var parameters = new NavigationParameters();
            parameters.Add("SelectedLocaton", new SearchedLocationModel());
            await Sut.InitializeAsync(parameters);
            Sut.IsFavourite = true;

            //Act
            Sut.UpdateFavouriteCommand.Execute(null);

            //Assert
            Mocker.GetMock<IBaseService>().Verify(x => x.PopupDialog.ShowPopup(AppConstants.SUCCESSANIMATION, AppConstants.SUCCESSHEADER, $"{Sut.WeatherLocation} {AppConstants.SUCCESSREMOVELOCATIONFAVOURITE}"), 
                                            Times.Once);
            Sut.IsFavourite.Should().BeFalse();
        }

        [Fact]
        public async Task UpdateFavourite_ShouldFail_WhenDeletingFavouriteFailed()
        {
            //Arrange
            Sut.WeatherLocation = "Sydney";

            Mocker.GetMock<IWeatherService>().Setup(x => x.DeleteFavourite(It.IsAny<SearchedLocationModel>()))
                                             .ReturnsAsync(false);

            var parameters = new NavigationParameters();
            parameters.Add("SelectedLocaton", new SearchedLocationModel());
            await Sut.InitializeAsync(parameters);
            Sut.IsFavourite = true;

            //Act
            Sut.UpdateFavouriteCommand.Execute(null);

            //Assert
            Mocker.GetMock<IBaseService>().Verify(x => x.PopupDialog.ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORHEADER, $"{Sut.WeatherLocation} {AppConstants.ERRORREMOVELOCATIONFAVOURITE}"),
                                            Times.Once);
            Sut.IsFavourite.Should().BeTrue();
        }

        [Fact]
        public async Task UpdateFavourite_ShouldBeFavourite_WhenClickngSaveFavourite()
        {
            //Arrange
            var weatherName = "Sydney";
            Sut.WeatherLocation = weatherName;

            Mocker.GetMock<IWeatherService>().Setup(x => x.CreateFavourite(It.IsAny<SearchedLocationModel>()))
                                             .ReturnsAsync(true);

            var parameters = new NavigationParameters();
            parameters.Add("SelectedLocaton", new SearchedLocationModel { Name = weatherName });
            await Sut.InitializeAsync(parameters);
            Sut.IsFavourite = false;

            //Act
            Sut.UpdateFavouriteCommand.Execute(null);

            //Assert
            Mocker.GetMock<IBaseService>().Verify(x => x.PopupDialog.ShowPopup(AppConstants.FAVOURITEANIMATION, AppConstants.FAVOURITEHEADER, $"{Sut.WeatherLocation} {AppConstants.SUCCESSLOCATIONFAVOURITE}"), 
                                            Times.Once);
            Sut.IsFavourite.Should().BeTrue();
        }

        [Fact]
        public async Task UpdateFavourite_ShouldTrimName_WhenNameHasComma()
        {
            //Arrange
            var weatherName = "Sydney,";
            Sut.WeatherLocation = weatherName;

            Mocker.GetMock<IWeatherService>().Setup(x => x.CreateFavourite(It.IsAny<SearchedLocationModel>()))
                                             .ReturnsAsync(true);

            var thisLocation = new SearchedLocationModel { Name = weatherName };
            var parameters = new NavigationParameters();
            parameters.Add("SelectedLocaton", thisLocation);
            await Sut.InitializeAsync(parameters);
            Sut.IsFavourite = false;

            //Act
            Sut.UpdateFavouriteCommand.Execute(null);

            //Assert
            thisLocation.Name.Should().Be("Sydney");
        }

        [Fact]
        public async Task UpdateFavourite_ShouldNotTrimName_WhenNameDoesNotHaveAComma()
        {
            //Arrange
            var weatherName = "Sydney";
            Sut.WeatherLocation = weatherName;

            Mocker.GetMock<IWeatherService>().Setup(x => x.CreateFavourite(It.IsAny<SearchedLocationModel>()))
                                             .ReturnsAsync(true);

            var thisLocation = new SearchedLocationModel { Name = weatherName };
            var parameters = new NavigationParameters();
            parameters.Add("SelectedLocaton", thisLocation);
            await Sut.InitializeAsync(parameters);
            Sut.IsFavourite = false;

            //Act
            Sut.UpdateFavouriteCommand.Execute(null);

            //Assert
            thisLocation.Name.Should().Be("Sydney");
        }

        [Fact]
        public async Task UpdateFavourite_ShouldFail_WhenCreatingFavouriteFailed()
        {
            //Arrange
            var weatherName = "Sydney";
            Sut.WeatherLocation = weatherName;

            Mocker.GetMock<IWeatherService>().Setup(x => x.CreateFavourite(It.IsAny<SearchedLocationModel>()))
                                             .ReturnsAsync(false);

            var parameters = new NavigationParameters();
            parameters.Add("SelectedLocaton", new SearchedLocationModel { Name = weatherName });
            await Sut.InitializeAsync(parameters);
            Sut.IsFavourite = false;

            //Act
            Sut.UpdateFavouriteCommand.Execute(null);

            //Assert
            Mocker.GetMock<IBaseService>().Verify(x => x.PopupDialog.ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORHEADER, $"{Sut.WeatherLocation} {AppConstants.ERRORLOCATIONFAVOURITE}"),
                                            Times.Once);
            Sut.IsFavourite.Should().BeFalse();
        }

        [Fact]
        public async Task InitializeAsync_ShouldHaveFavouriteLocation_WhenFavouriteLocationIsPassed()
        {
            //Arrange
            var Id = 12;
            var weatherName = "Sydney";

            var userFavourites = new List<SearchedLocationModel>
            {
                new SearchedLocationModel { Name = weatherName, Id = Id}
            };

            Mocker.GetMock<ICurrentUser>().Setup(x => x.favourites)
                                        .Returns(userFavourites);

            Mocker.GetMock<IWeatherService>().Setup(x => x.GetForecast(It.IsAny<string>()))
                                 .ReturnsAsync(new WeatherModel());

            var thisLocation = new SearchedLocationModel { Name = weatherName, Id = Id };

            var parameters = new NavigationParameters();
            parameters.Add("SelectedLocaton", thisLocation);
            parameters.Add("LocationData", weatherName);

            //Act
            await Sut.InitializeAsync(parameters);

            //Assert
            Sut.IsFavourite.Should().BeTrue();
        }

        [Fact]
        public async Task InitializeAsync_ShouldNotHaveFavouriteLocation_WhenNonFavouriteLocationIsPassed()
        {
            //Arrange
            var Id = 12;
            var locationId = 34;
            var weatherName = "Sydney";

            var userFavourites = new List<SearchedLocationModel>
            {
                new SearchedLocationModel { Name = weatherName, Id = Id}
            };

            Mocker.GetMock<ICurrentUser>().Setup(x => x.favourites)
                                        .Returns(userFavourites);

            Mocker.GetMock<IWeatherService>().Setup(x => x.GetForecast(It.IsAny<string>()))
                                 .ReturnsAsync(new WeatherModel());

            var thisLocation = new SearchedLocationModel { Name = weatherName, Id = locationId };

            var parameters = new NavigationParameters();
            parameters.Add("SelectedLocaton", thisLocation);
            parameters.Add("LocationData", weatherName);

            //Act
            await Sut.InitializeAsync(parameters);

            //Assert
            Sut.IsFavourite.Should().BeFalse();
        }

        [Fact]
        public async Task InitializeAsync_ShouldGetFavourite_WhenSelectingAFavourite()
        {
            //Arrange
            var weatherName = "Sydney";

            var locationData = new WeatherModel
            {
                Location = new WeatherLocationModel
                {
                    Name = weatherName
                }
            };

            var userFavourite = new SearchedLocationModel
            { 
                Name = weatherName
            };

            var userFavourites = new List<SearchedLocationModel>();
            userFavourites.Add(userFavourite);

            Mocker.GetMock<ICurrentUser>().Setup(x => x.favourites)
                                        .Returns(userFavourites);

            Mocker.GetMock<IWeatherService>().Setup(x => x.GetForecast(It.IsAny<string>()))
                                 .ReturnsAsync(locationData);

            var parameters = new NavigationParameters();
            parameters.Add("SelectedLocaton", null);
            parameters.Add("LocationData", weatherName);

            //Act
            await Sut.InitializeAsync(parameters);

            //Assert
            Sut.IsFavourite.Should().Be(true);
        }

        [Fact]
        public async Task InitializeAsync_ShouldFindLocation_WhenLocationIsNotAFavourite()
        {
            //Arrange
            var lon = 22;
            var lat = 112;
            var weatherName = "Sydney";
            var weatherNameCaringbah = "Caringbah";
            var weatherNameSutherland = "Sutherland";

            var locationData = new WeatherModel
            {
                Location = new WeatherLocationModel
                {
                    Lon = lon,
                    Lat = lat,
                    Name = weatherName
                }
            };

            var userFavourites = new List<SearchedLocationModel>();

            var searchedLocations =  new List<SearchedLocationModel> 
            {
                new SearchedLocationModel {Name = weatherName},
                new SearchedLocationModel {Name = weatherNameCaringbah},
                new SearchedLocationModel {Name = weatherNameSutherland}
            };

            Mocker.GetMock<ICurrentUser>().Setup(x => x.favourites)
                                        .Returns(userFavourites);

            Mocker.GetMock<IWeatherService>().Setup(x => x.GetForecast(It.IsAny<string>()))
                                 .ReturnsAsync(locationData);

            Mocker.GetMock<IWeatherService>().Setup(x => x.GetSearchedLocatons(It.IsAny<string>()))
                     .ReturnsAsync(searchedLocations);

            SearchedLocationModel thisLocation = null;
            var parameters = new NavigationParameters();
            parameters.Add("SelectedLocaton", thisLocation);
            parameters.Add("LocationData", weatherName);

            //Act
            await Sut.InitializeAsync(parameters);

            //Assert
            Sut.IsFavourite.Should().BeFalse();
        }
    }
}
