﻿using CoreWeatherAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Data
{
    public interface IWeatherContextService
    {
        Task<IEnumerable<DbSearchedLocation>> LoadLocationsBySearch(string q);
        Task<DbWeather> LoadWeatherBySearch(string q);
        Task<DbWeather> LoadForecastByLocation(string q);
    }
}