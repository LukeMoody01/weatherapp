﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Firebase.Messaging;
using Prism;
using Prism.Ioc;
using Prism.Navigation;
using System.Diagnostics;
using Shiny;
using System.Threading.Tasks;
using Xamarin.Essentials;
using static Android.App.ActivityManager;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace PrismWeatherApp.Droid
{
    [IntentFilter(new[] { Intent.ActionView },
        Categories = new[] { Intent.CategoryDefault, Intent.CategoryBrowsable },
        DataScheme = "http",
        DataHost = "moodyweather")]
    [Activity(Theme = "@style/MainTheme",
              ConfigurationChanges =  ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize, LaunchMode = LaunchMode.SingleTop)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected async override void OnCreate(Bundle savedInstanceState)
        {
            //TabLayoutResource = Resource.Layout.Tabbar;
            //ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(savedInstanceState);
            Rg.Plugins.Popup.Popup.Init(this);
            AppCenter.Start("108e4b9c-27ed-443b-82e2-c056214f88fa",
                   typeof(Analytics), typeof(Crashes));

            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            FirebaseMessaging.Instance.SubscribeToTopic("weather");
            bool flag = false;
            if (Intent != null && Intent.DataString != null) 
            {
                System.Diagnostics.Debug.WriteLine("We found Intent");
                flag = true;
                await SaveIntentData(Intent);
            }

            LoadApplication(new App(flag, new AndroidInitializer()));
        }

        protected async override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            await SaveIntentData(intent);

            RunningAppProcessInfo myProcess = new RunningAppProcessInfo();
            GetMyMemoryState(myProcess);
            if (myProcess.Importance == Importance.Foreground || myProcess.Importance == Importance.Background)
            {
                await NavigateToWeatherPage();
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public override void OnBackPressed()
        {
            Rg.Plugins.Popup.Popup.SendBackPressed(base.OnBackPressed);
        }
        private async Task NavigateToWeatherPage()
        {
            var _navigationService = ContainerLocator.Container.Resolve<INavigationService>();
            var navP = new NavigationParameters();
            navP.Add("LocationData", Preferences.Get("LocationData", "Sydney"));
            navP.Add("SharedLinkData", Preferences.Get("SharedLinkData", false));
            navP.Add("FromNotification", Preferences.Get("FromNotification", false));
            await _navigationService.NavigateAsync($"/MainTabPage?{KnownNavigationParameters.SelectedTab}=WeatherPage", navP);
        }
        private async Task<bool> SaveIntentData(Intent intent)
        {
            bool fromSharedLink = false;
            bool fromNotification = true;
            string locationName = null;
            if (intent.Extras != null)
            {
                locationName = intent.Extras.GetString("LocationName"); // Notification
            }

            if (locationName == null)
            {
                locationName = intent.Data.GetQueryParameter("LocationName"); // Deep link
                fromSharedLink = true;
                fromNotification = false;
            }
            Preferences.Set("LocationData", locationName);
            Preferences.Set("SharedLinkData", fromSharedLink);
            Preferences.Set("FromNotification", fromNotification);
            return true;
        }
    }

    public class AndroidInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            // Register any platform specific implementations
        }
    }
}

