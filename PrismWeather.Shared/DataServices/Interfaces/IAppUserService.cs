﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Shared.Models;

namespace WeatherApp.Shared.Services.Interfaces
{
    public interface IAppUserService
    {
        Task<string> GetToken(LoginRequestModel loginRequest);
        Task<bool> CreateUser(UserModel user);

        Task<UserModel> GetUser(LoginRequestModel loginRequest);
        Task<int> AddUserToCache(UserModel user);
        Task<UserModel> GetUserFromCache(string userID);
        Task<bool> ResetPassword(string email);
        Task<UserModel> GetExistingUser(string email);
    }
}
