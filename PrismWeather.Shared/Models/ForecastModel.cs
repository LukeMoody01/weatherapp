﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherApp.Shared.Models
{
    public class ForecastModel
    {
        public List<ForecastDayModel> Forecastday { get; set; }
    }
}
