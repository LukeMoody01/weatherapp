﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace WeatherApp.Services
{
    // This helper is for us to easily use and add data to the secure storage
    public class SecureStorageHelper : ISecureStorageHelper
    {
        #region Public Methods

        public bool DeleteKey(string key)
        {
            try
            {
                var success = SecureStorage.Remove(key);
                if (success)
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

            Preferences.Remove(key);
            return true;
        }

        public void DeleteAllKey()
        {
            try
            {
                SecureStorage.RemoveAll();
                return;
            }
            catch (Exception)
            {
            }

            Preferences.Clear();
        }

        public async Task<string> GetValue(string key, string defaultValue = null)
        {
            try
            {
                var value = await SecureStorage.GetAsync(key);
                if (value == null) return defaultValue;
                return value;
            }
            catch (Exception)
            {
            }

            //If the device doesn't support secure storage use preferences instead
            return Preferences.Get(key, defaultValue);
        }

        public async Task SetValue(string key, string value)
        {
            try
            {
                await SecureStorage.SetAsync(key, value);
                return;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            //If the device doesn't support secure storage use preferences instead
            Preferences.Set(key, value);
        }

        #endregion
    }
}
