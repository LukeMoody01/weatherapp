﻿using System.ComponentModel.DataAnnotations;

namespace CoreWeatherAPI.Models
{
    public class UserModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string EmailAddress { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
