﻿using AutoMapper;
using CoreWeatherAPI.Data;
using CoreWeatherAPI.Models;
using CoreWeatherAPI.TokenAuthentication;

namespace CoreWeatherAPI.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CurrentWeather, DbCurrentWeather>();
            CreateMap<SearchedLocation, DbSearchedLocation>();
            CreateMap<UserModel, DbUserModel>();
            CreateMap<Weather, DbWeather>();
            CreateMap<WeatherCondition, DbWeatherCondition>();
            CreateMap<WeatherLocation, DbWeatherLocation>();
            CreateMap<Astro, DbAstro>();
            CreateMap<Day, DbDay>();
            CreateMap<Forecast, DbForecast>();
            CreateMap<ForecastDay, DbForecastday>();
            CreateMap<DbHour, Hour>();
        }
    }
}
