﻿using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using Prism.Navigation.TabbedPages;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using WeatherApp.Services;
using WeatherApp.Shared.Authentication;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.Events;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Services.Interfaces;

namespace PrismWeatherApp.ViewModels
{
    public class WeatherSearchPageViewModel : ViewModelBase
    {
        #region Fields
        private readonly IWeatherService _weatherService;
        private readonly ICurrentUser _appUser;
        private readonly IAuthenticateService _logoutService;
        private ObservableCollection<SearchedLocationModel> _usersFavouriteLocations;
        private SearchedLocationModel _selectedLocation;
        #endregion

        #region Properties
        public string SearchLocation { get; set; }
        public ObservableCollection<SearchedLocationModel> UsersFavouriteLocations
        {
            get { return _usersFavouriteLocations; }
            set
            {
                SetProperty(ref _usersFavouriteLocations, value);
            }
        }
        public SearchedLocationModel SelectedLocation
        {
            get { return _selectedLocation; }
            set
            {
                SetProperty(ref _selectedLocation, value);
                SelectFavouriteCommand.Execute(_selectedLocation);
            }
        }
        #endregion

        #region Commands
        public ICommand SelectFavouriteCommand { get; set; }
        public ICommand SearchLocationCommand { get; set; }
        public ICommand LogOutCommand { get; set; }
        #endregion

        #region Ctor
        public WeatherSearchPageViewModel(IBaseService baseService,
            IWeatherService weatherService,
            ICurrentUser appUser,
            IEventAggregator eventAggregator,
            IAuthenticateService logoutService) : base(baseService)
        {
            _weatherService = weatherService;
            _appUser = appUser;
            _logoutService = logoutService;
            PopulateData();

            SearchLocationCommand = new DelegateCommand(async () => await SearchGivenLocation());
            SelectFavouriteCommand = new DelegateCommand<SearchedLocationModel>(NavigateToFavouriteLocation);
            LogOutCommand = new DelegateCommand(async () => await LogOut());
            eventAggregator.GetEvent<FavouriteChangedEvent>().Subscribe(PopulateData);
        }
        #endregion

        #region Methods
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            IsBusy = false;
            SelectedLocation = null;
            PopulateData();
        }

        private void PopulateData()
        {
            UsersFavouriteLocations = new ObservableCollection<SearchedLocationModel>(_appUser.favourites);
        }

        private async Task LogOut()
        {
            IsBusy = true;
            await _logoutService.Logout();
            await _navigationService.NavigateAsync("/LoginPage");
            IsBusy = false;
        }

        private async void NavigateToFavouriteLocation(SearchedLocationModel searchedLocation)
        {
            if (SelectedLocation == null)
                return;

            if (!CanClick(1000))
                return;

            IsBusy = true;

            var parameteres = new NavigationParameters();
            parameteres.Add("SelectedLocaton", SelectedLocation);
            parameteres.Add("LocationData", $"{SelectedLocation.Name}-{SelectedLocation.Region}");

            await _navigationService.SelectTabAsync("WeatherPage", parameteres);
            SelectedLocation = null;
            IsBusy = false;
        }

        private async Task SearchGivenLocation()
        {
            if (!CanClick(1000))
                return;

            if (string.IsNullOrEmpty(SearchLocation))
                return;

            IsBusy = true;

            var searchedLocations = await _weatherService.GetSearchedLocatons(SearchLocation);

            if (searchedLocations.Count() == 0)
            {
                await ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORSERACHHEADER, AppConstants.ERRORSEARCHNOTVALID);
                IsBusy = false;
                return;
            }

            var parameters = new NavigationParameters();
            parameters.Add("SearchedLocations", searchedLocations);

            await _navigationService.SelectTabAsync("SelectLocationPage", parameters);
            IsBusy = false;
        }
        #endregion
    }
}
