﻿namespace WeatherApp.Utility
{
    public interface ILocalDBUtility
    {
        string GetDBPath(string filename);
    }
}
