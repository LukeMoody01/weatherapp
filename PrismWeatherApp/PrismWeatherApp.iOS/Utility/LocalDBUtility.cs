﻿using System;
using System.IO;
using WeatherApp.Utility;

namespace PrismWeatherApp.iOS.Utility
{
    public class LocalDBUtility : ILocalDBUtility
    {
        public LocalDBUtility()
        {

        }

        public string GetDBPath(string filename)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
    }
}
