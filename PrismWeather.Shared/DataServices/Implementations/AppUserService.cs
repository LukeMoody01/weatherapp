﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Shared.GatewayAccess.Interfaces;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Repository.Interfaces;
using WeatherApp.Shared.Services.Interfaces;

namespace WeatherApp.Shared.Services.Implementations
{
    public class AppUserService : IAppUserService
    {
        private readonly IAppUserRepository _appUserRepository;
        private readonly IAppUserGateway _appUserGateway;

        public AppUserService(IAppUserGateway appUserGateway, 
            IAppUserRepository appUserRepository)
        {
            _appUserGateway = appUserGateway;
            _appUserRepository = appUserRepository;
        }

        public async Task<string> GetToken(LoginRequestModel loginRequest)
        {
            return await _appUserGateway.GetToken(loginRequest);
        }

        public async Task<bool> CreateUser(UserModel user)
        {
            return await _appUserGateway.PostCreateUser(user);
        }

        public async Task<UserModel> GetExistingUser(string email)
        {
            return await _appUserGateway.GetUserForRegistration(email);
        }

        public async Task<bool> ResetPassword(string email)
        {
            return await _appUserGateway.PostResetPassword(email);
        }

        public async Task<UserModel> GetUser(LoginRequestModel loginRequest)
        {
            return await _appUserGateway.Login(loginRequest);
        }

        public async Task<int> AddUserToCache(UserModel user)
        {
            var result = await _appUserRepository.SaveUser(user);

            return result;
        }

        public async Task<UserModel> GetUserFromCache(string userID)
        {
            var result = await _appUserRepository.GetUser(userID);

            return result;

        }
    }
}
