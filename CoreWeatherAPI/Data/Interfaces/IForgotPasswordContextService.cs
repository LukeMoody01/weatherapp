﻿using System.Threading.Tasks;

namespace CoreWeatherAPI.Data
{
    public interface IForgotPasswordContextService
    {
        DbForgottenPassword GetForgotPasswordRequest(string reqId);
        Task<bool> UpdateRequest(DbForgottenPassword forgotPassword);
    }
}