﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PrismWeatherApp.Views.Controls
{
    public class SearchFrame : Frame
    {
        public static readonly BindableProperty FramePadding =
            BindableProperty.Create("Padding", typeof(int), typeof(SearchFrame), 0);

        public int SearchFramePadding
        {
            get { return (int)GetValue(FramePadding); }
            set { SetValue(FramePadding, value); }
        }
    }
}
