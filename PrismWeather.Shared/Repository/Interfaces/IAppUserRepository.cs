﻿using System.Threading.Tasks;
using WeatherApp.Shared.Models;

namespace WeatherApp.Shared.Repository.Interfaces
{
    public interface IAppUserRepository
    {
        Task<int> SaveUser(UserModel user);
        Task<UserModel> GetUser(string userId);
    }
}
