﻿using CoreWeatherAPI.Data;
using CoreWeatherAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Controllers
{
    public class ForgottenPasswordController : Controller
    {
        private readonly IUserContextService _userContextService;
        private readonly IForgotPasswordContextService _passwordContextService;

        public ForgottenPasswordController(IUserContextService userContextService,
            IForgotPasswordContextService passwordContextService)
        {
            _userContextService = userContextService;
            _passwordContextService = passwordContextService;
        }

        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Submit()
        {
            string password = Request.Form["NewPassword"];
            string confirmedPassword = Request.Form["ConfirmPassword"];
            string reqId = Request.Form["ReqId"];

            if (password != confirmedPassword)
                return BadRequest();

            var validRequest = _passwordContextService.GetForgotPasswordRequest(reqId);
            if (validRequest == null)
                return BadRequest();

            var user = _userContextService.GetUserById(validRequest.UserId);
            await _userContextService.ResetPassword(confirmedPassword, user.Id);
            validRequest.IsFulfilled = true;
            var result = await _passwordContextService.UpdateRequest(validRequest);
            return Ok(result);
        }
    }
}
