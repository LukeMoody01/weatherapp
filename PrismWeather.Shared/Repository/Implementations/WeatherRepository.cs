﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Repository.Interfaces;

namespace WeatherApp.Shared.Repository.Implementations
{
    public class WeatherRepository : Repository<SearchedLocationModel>, IWeatherRepository
    {
        public WeatherRepository(Lazy<IDBConnection> lazyDBConnection) : base(lazyDBConnection)
        {
        }

        public async Task<IEnumerable<SearchedLocationModel>> GetFavourites(string userId)
        {
            IEnumerable<SearchedLocationModel> result = null;
            try
            {
                if (userId != null)
                {
                    result = await Get<IEnumerable<SearchedLocationModel>>(x => x.UserFavourited == userId);
                }
            }
            catch (SQLiteException e)
            {
                throw e;
            }
            return result;
        }

        public async Task<bool> SaveFavourites(IEnumerable<SearchedLocationModel> favourites)
        {
            try
            {
                var result = await InsertAllItems(favourites);
            }
            catch (SQLiteException)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> SaveFavourite(SearchedLocationModel favourite)
        {
            try
            {
                var result = await InsertItem(favourite);
            }
            catch (SQLiteException)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> RemoveFavourite(SearchedLocationModel favourite)
        {
            try
            {
                var result = await DeleteItem(favourite);
            }
            catch (SQLiteException)
            {
                return false;
            }
            return true;
        }
    }
}
