﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Shared.Authentication;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Repository.Interfaces;
using WeatherApp.Shared.Services.Interfaces;

namespace WeatherApp.Services
{
    public class AuthenticateService : IAuthenticateService
    {
        private readonly ICurrentUser _appUser;
        private readonly ISecureStorageHelper _secureStorage;
        private readonly IDBConnection _dBConnection;
        private readonly IDatabase _database;
        private readonly IAppUserService _appUserService;
        private readonly IWeatherService _weatherService;
        private readonly IAuthenticateManager _authenticateManager;
        private readonly IPopupDialog _popupDialog;

        public AuthenticateService(ICurrentUser appUser,
            ISecureStorageHelper secureStorage,
            IDBConnection dBConnection,
            IDatabase database,
            IAppUserService appUserService,
            IWeatherService weatherService,
            IAuthenticateManager authenticateManager,
            IPopupDialog popupDialog)
        {
            _appUser = appUser;
            _secureStorage = secureStorage;
            _dBConnection = dBConnection;
            _database = database;
            _appUserService = appUserService;
            _weatherService = weatherService;
            _authenticateManager = authenticateManager;
            _popupDialog = popupDialog;
        }

        public async Task<bool> Login(LoginRequestModel loginRequest)
        {
            string token = await _appUserService.GetToken(loginRequest);

            if (string.IsNullOrEmpty(token))
            {
                return false;
            }
            _authenticateManager.ForceSetToken(token);

            var user = await _appUserService.GetUser(loginRequest);

            await InitializeDB();

            await SetAppUser(user);

            await SetStorageValues(token, user.Id);
            return true;
        }

        public async Task Logout()
        {
            _secureStorage.DeleteAllKey();
            await _dBConnection.DeleteDatabaseFile();
            _appUser.favourites = null;
            _appUser.person = null;
            _authenticateManager.ForceSetToken(string.Empty);
        }

        public async Task<bool> IsUserLoggedIn()
        {
            var key = await _secureStorage.GetValue("token");
            if (key == null)
            {
                return false;
            }
            _authenticateManager.ForceSetToken(key);
            return true;
        }

        private async Task SetAppUser(UserModel user)
        {
            var favourites = await _weatherService.GetFavourites(user.Id);
            _appUser.favourites = new List<SearchedLocationModel>(favourites);
            _appUser.person = user;
            await _appUserService.AddUserToCache(user);
            await _weatherService.AddFavouritesToCache(favourites);
        }

        private async Task SetStorageValues(string token, string userId)
        {
            _secureStorage.DeleteAllKey();
            await _secureStorage.SetValue("token", token);
            await _secureStorage.SetValue("user", userId);
        }

        private async Task InitializeDB()
        {
            await _dBConnection.DeleteDatabaseFile();

            _dBConnection.CreateDBConnection();

            await _database.InitializeDatabase();
        }
    }
}
