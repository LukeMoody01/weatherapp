﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Shared.Models;

namespace WeatherApp.Shared.GatewayAccess.Interfaces
{
    public interface IWeatherGateway
    {
        Task<WeatherModel> GetWeather(string Location);

        Task<IEnumerable<SearchedLocationModel>> GetSearchedLocations(string Location);
        Task<WeatherModel> GetForecast(string Location);
        Task<bool> CreateFavourite(SearchedLocationModel favourite);
        Task<bool> RemoveFavourite(SearchedLocationModel favourite);
        Task<IEnumerable<SearchedLocationModel>> GetFavourites(string userId);
    }
}
