﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Shared.Authentication;
using WeatherApp.Shared.GatewayAccess.Interfaces;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Repository.Interfaces;
using WeatherApp.Shared.Services.Interfaces;

namespace WeatherApp.Shared.Services.Implementations
{
    public class WeatherService : IWeatherService
    {
        private readonly IWeatherGateway _weatherGateway;
        private readonly IWeatherRepository _weatherRepository;
        private readonly ICurrentUser _currentUser;

        public WeatherService(IWeatherGateway weatherGateway,
            IWeatherRepository weatherRepository,
            ICurrentUser currentUser)
        {
            _weatherGateway = weatherGateway;
            _weatherRepository = weatherRepository;
            _currentUser = currentUser;
        }

        public async Task<bool> CreateFavourite(SearchedLocationModel favourite)
        {
            await AddFavouriteToCache(favourite);

            if (await _weatherGateway.CreateFavourite(favourite))
            {
                _currentUser.favourites.Add(favourite);
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteFavourite(SearchedLocationModel favourite)
        {
            await RemoveFavouriteFromCache(favourite);

            if (await _weatherGateway.RemoveFavourite(favourite))
            {
                var favouriteToRemove = _currentUser.favourites.Find(f => f.UserFavourited == favourite.UserFavourited && f.Id == favourite.Id);
                _currentUser.favourites.Remove(favouriteToRemove);
                return true;
            }
            return false;
        }

        public async Task<IEnumerable<SearchedLocationModel>> GetFavourites(string userId)
        {
            return await _weatherGateway.GetFavourites(userId);
        }

        public async Task<WeatherModel> GetWeather(string location)
        {
            var weather = await _weatherGateway.GetWeather(location);

            return weather;
        }

        public async Task<WeatherModel> GetForecast(string location)
        {
            var weather = await _weatherGateway.GetForecast(location);

            return weather;
        }

        public async Task<List<SearchedLocationModel>> GetSearchedLocatons(string Location)
        {
            var weather = await _weatherGateway.GetSearchedLocations(Location);
            if (weather == null)
                weather = new List<SearchedLocationModel>();
            return new List<SearchedLocationModel>(weather);
        }

        public async Task<IEnumerable<SearchedLocationModel>> GetFavouritesFromCache(string userId)
        {
            var weather = await _weatherRepository.GetFavourites(userId);

            return weather;
        }

        public async Task<bool> AddFavouritesToCache(IEnumerable<SearchedLocationModel> favourites)
        {
            var weather = await _weatherRepository.SaveFavourites(favourites);

            return weather;
        }

        public async Task<bool> AddFavouriteToCache(SearchedLocationModel favourite)
        {
            var weather = await _weatherRepository.SaveFavourite(favourite);

            return weather;
        }

        public async Task<bool> RemoveFavouriteFromCache(SearchedLocationModel favourite)
        {
            var weather = await _weatherRepository.RemoveFavourite(favourite);

            return weather;
        }
    }
}
