﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.GatewayAccess.Base;
using WeatherApp.Shared.GatewayAccess.Interfaces;
using WeatherApp.Shared.Models;

namespace WeatherApp.Shared.GatewayAccess.Implementations
{
    public class WeatherGateway : GatewayServiceBase, IWeatherGateway
    {
        private HttpClient httpClient { get; set; }

        private string _getWeatherRequest = null;
        private const string GET_WEATHER_REQUEST = "current/{0}";

        private string _getLocationsRequest = null;
        private const string GET_LOCATIONS_REQUEST = "search/{0}";

        private string _getForcastRequest = null;
        private const string GET_FORCAST_REQUEST = "forecast/{0}";

        private string _postFavouriteRequest = null;
        private const string POST_FAVOURITE_REQUEST = "createfavourite";

        private string _deleteFavouriteRequest = null;
        private const string DELETE_FAVOURITE_REQUEST = "deletefavourite";

        private string _getUserFavourites = null;
        private const string GET_USER_FAVOURITES = "getfavourites/{0}";

        public WeatherGateway(IAppSettingsProvider appSettings) : base(appSettings)
        {
            httpClient = GetClient();

            InitBaseAPI();
        }

        public async Task<bool> CreateFavourite(SearchedLocationModel favourite)
        {
            var responseObject = await PostRemoteAsync<bool>(_postFavouriteRequest, CastToStringContent<SearchedLocationModel>(favourite));
            return responseObject;
        }

        public async Task<bool> RemoveFavourite(SearchedLocationModel favourite)
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Delete, _deleteFavouriteRequest);
                request.Content = new StringContent(JsonConvert.SerializeObject(favourite), Encoding.UTF8, "application/json");
                using HttpResponseMessage response = await httpClient.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        return bool.Parse(data);
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<SearchedLocationModel>> GetFavourites(string userId)
        {
            var enpoint = string.Format(_getUserFavourites, userId);
            var responseObject = await GetRemoteAsync<IEnumerable<SearchedLocationModel>>(enpoint);
            return responseObject;
        }

        public async Task<WeatherModel> GetWeather(string Location)
        {
            var enpoint = string.Format(_getWeatherRequest, Location);
            var responseObject = await GetRemoteAsync<WeatherModel>(enpoint);
            return responseObject;
        }

        public async Task<WeatherModel> GetForecast(string Location)
        {
            var enpoint = string.Format(_getForcastRequest, Location);
            var responseObject = await GetRemoteAsync<WeatherModel>(enpoint);
            return responseObject;
        }
        public async Task<IEnumerable<SearchedLocationModel>> GetSearchedLocations(string location)
        {
            var enpoint = string.Format(_getLocationsRequest, location);
            var responseObject = await GetRemoteAsync<IEnumerable<SearchedLocationModel>>(enpoint);
            return responseObject;
        }

        private void InitBaseAPI()
        {
            var _baseAPI = BaseAPI;

            _getWeatherRequest = _baseAPI + GET_WEATHER_REQUEST;
            _getLocationsRequest = _baseAPI + GET_LOCATIONS_REQUEST;
            _getForcastRequest = _baseAPI + GET_FORCAST_REQUEST;
            _postFavouriteRequest = _baseAPI + POST_FAVOURITE_REQUEST;
            _deleteFavouriteRequest = _baseAPI + DELETE_FAVOURITE_REQUEST;
            _getUserFavourites = _baseAPI + GET_USER_FAVOURITES;
        }
    }
}
