﻿using Newtonsoft.Json;

namespace WeatherApp.Shared.Models
{
    public class WeatherModel
    {
        [JsonProperty("location")]
        public WeatherLocationModel Location { get; set; }

        [JsonProperty("current")]
        public CurrentWeatherModel Current { get; set; }
        [JsonProperty("forecast")]
        public ForecastModel Forecast { get; set; }
    }
}
