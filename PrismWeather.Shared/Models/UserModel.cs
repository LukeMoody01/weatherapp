﻿namespace WeatherApp.Shared.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }

    }
}
