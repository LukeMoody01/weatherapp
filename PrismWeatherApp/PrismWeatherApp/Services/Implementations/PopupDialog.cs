﻿using PrismWeatherApp.Views.Popups;
using Rg.Plugins.Popup.Services;
using System.Threading.Tasks;

namespace WeatherApp.Services
{
    public class PopupDialog : IPopupDialog
    {
        public async Task ShowPopup(string popupAnimationType, string popupHeader, string popupBody)
        {
            var page = new PopupPage(popupAnimationType, popupHeader, popupBody);
            await PopupNavigation.Instance.PushAsync(page);
        }
    }
}
