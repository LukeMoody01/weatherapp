﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.GatewayAccess.Base;
using WeatherApp.Shared.GatewayAccess.Interfaces;
using WeatherApp.Shared.Models;

namespace WeatherApp.Shared.GatewayAccess.Implementations
{
    public class AppUserGateway : GatewayServiceBase, IAppUserGateway
    {
        private HttpClient httpClient { get; set; }

        private string _postUserRequest = null;
        private const string POST_USER_REQUEST = "createuser";

        private string _getLoginUser = null;
        private const string GET_LOGIN_USER = "login";

        private string _getUserRequest = null;
        private const string GET_USER_REQUEST = "getuser";

        private string _getAuthenticatedRequest = null;
        private const string GET_AUTHENTICATED_REQUEST = "authenticateuser";

        private string _postResetPassword = null;
        private const string POST_RESET_PASSWORD = "resetpassword";

        public AppUserGateway(IAppSettingsProvider appSettingsProvider) : base(appSettingsProvider)
        {
            httpClient = GetClient();

            InitBaseAPI();
        }

        public async Task<bool> PostCreateUser(UserModel user)
        {
            var responseObject = await PostRemoteAsync<bool>(_postUserRequest, CastToStringContent<UserModel>(user));
            return responseObject;
        }

        public async Task<bool> PostResetPassword(string email)
        {
            var responseObject = await PostRemoteAsync<bool>(_postResetPassword, CastToStringContent<string>(email));
            return responseObject;
        }

        public async Task<string> GetToken(LoginRequestModel loginRequest)
        {
            try
            {
                string token = string.Empty;
                using (HttpResponseMessage response = await httpClient.PostAsync(_getAuthenticatedRequest, CastToStringContent<LoginRequestModel>(loginRequest)))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        token = await response.Content.ReadAsStringAsync();
                        return token;
                    }
                }
                return token;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<UserModel> Login(LoginRequestModel loginRequest)
        {
            var responseObject = await PostRemoteAsync<UserModel>(_getLoginUser, CastToStringContent<LoginRequestModel>(loginRequest));
            return responseObject;
        }

        public async Task<UserModel> GetUserForRegistration(string email)
        {
            var responseObject = await PostRemoteAsync<UserModel>(_getUserRequest, CastToStringContent<string>(email));
            return responseObject;
        }

        private void InitBaseAPI()
        {
            var _baseAPI = BaseAPI;

            _postUserRequest = _baseAPI + POST_USER_REQUEST;
            _getUserRequest = _baseAPI + GET_USER_REQUEST;
            _getAuthenticatedRequest = _baseAPI + GET_AUTHENTICATED_REQUEST;
            _postResetPassword = _baseAPI + POST_RESET_PASSWORD;
            _getUserRequest = _baseAPI + GET_USER_REQUEST;
            _getLoginUser = _baseAPI + GET_LOGIN_USER;
        }
    }
}
