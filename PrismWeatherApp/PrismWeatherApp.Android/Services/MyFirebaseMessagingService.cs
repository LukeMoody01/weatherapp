﻿using Android.App;
using Android.Content;
using Firebase.Messaging;

namespace PrismWeatherApp.Droid
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT"})]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        public MyFirebaseMessagingService()
        {

        }

        [System.Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override void OnMessageReceived(RemoteMessage message)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            base.OnMessageReceived(message);
            new NotificationHelper().CreateAlertForFavourite("Update", "User");
        }
    }
}