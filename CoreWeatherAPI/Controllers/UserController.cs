﻿using CoreWeatherAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoreWeatherAPI.Data;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using CoreWeatherAPI.Filters;

namespace CoreWeatherAPI.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserContextService _userContextService;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private string thisAPI;

        public UserController(IUserContextService userContextService,
            IMapper mapper,
            IConfiguration configuration)
        {
            _userContextService = userContextService;
            _mapper = mapper;
            _configuration = configuration;
            thisAPI = _configuration.GetValue<string>("ApiHost");
        }

        [HttpPost("createfavourite")]
        [TokenAuthenticationFilter]
        public async Task<IActionResult> CreateFavourite([FromBody] SearchedLocation favourite)
        {
            var mappedData = _mapper.Map<DbSearchedLocation>(favourite);
            var result = await _userContextService.CreateFavourite(mappedData);
            return Created($"{thisAPI}createfavourite",result);
        }

        [HttpGet("getfavourites/{userId}")]
        [TokenAuthenticationFilter]
        public IActionResult GetFavourites(string userId)
        {
            IEnumerable<DbSearchedLocation> locations = _userContextService.GetFavourites(userId);
            return Ok(locations);
        }

        [HttpDelete("deletefavourite")]
        [TokenAuthenticationFilter]
        public async Task<IActionResult> DeleteFavourite([FromBody] SearchedLocation favourite)
        {
            var mappedData = _mapper.Map<DbSearchedLocation>(favourite);
            var result = await _userContextService.DeleteFavourite(mappedData);
            return Ok(result);
        }
    }
}
