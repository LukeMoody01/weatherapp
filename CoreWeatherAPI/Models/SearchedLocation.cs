﻿namespace CoreWeatherAPI.Models
{
    public class SearchedLocation
    {
        public string PrimaryId { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Region { get; set; }

        public string Country { get; set; }

        public double Lat { get; set; }

        public double Lon { get; set; }

        public string Url { get; set; }

        public string UserFavourited { get; set; }
    }
}
