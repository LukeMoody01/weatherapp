﻿using Newtonsoft.Json;

namespace CoreWeatherAPI.Data
{
    public class DbWeather
    {
        [JsonProperty("location")]
        public DbWeatherLocation Location { get; set; }

        [JsonProperty("current")]
        public DbCurrentWeather Current { get; set; }

        [JsonProperty("forecast")]
        public DbForecast Forecast { get; set; }
    }
}
