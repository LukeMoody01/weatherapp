﻿using Xamarin.Forms;

namespace PrismWeatherApp.Views.Controls
{
    public class BorderlessEntry : Entry
    {

        public static readonly BindableProperty BorderWidthProperty =
            BindableProperty.Create("BorderWidth", typeof(int), typeof(BorderlessEntry), 0);

        public int EntryBorderWidth
        {
            get { return (int)GetValue(BorderWidthProperty); }
            set { SetValue(BorderWidthProperty, value); }
        }
    }
}
