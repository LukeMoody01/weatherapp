﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Shared.Models;

namespace WeatherApp.Shared.Services.Interfaces
{
    public interface IWeatherService
    {
        Task<WeatherModel> GetWeather(string location);

        Task<List<SearchedLocationModel>> GetSearchedLocatons(string location);
        Task<WeatherModel> GetForecast(string location);
        Task<IEnumerable<SearchedLocationModel>> GetFavouritesFromCache(string userId);
        Task<bool> AddFavouritesToCache(IEnumerable<SearchedLocationModel> favourites);
        Task<bool> AddFavouriteToCache(SearchedLocationModel favourite);
        Task<bool> RemoveFavouriteFromCache(SearchedLocationModel favourite);
        Task<bool> CreateFavourite(SearchedLocationModel favourite);
        Task<bool> DeleteFavourite(SearchedLocationModel favourite);
        Task<IEnumerable<SearchedLocationModel>> GetFavourites(string userId);
    }
}
