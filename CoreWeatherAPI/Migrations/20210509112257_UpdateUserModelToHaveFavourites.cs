﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreWeatherAPI.Migrations
{
    public partial class UpdateUserModelToHaveFavourites : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DbWeatherLocation",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Region = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Lat = table.Column<double>(type: "float", nullable: false),
                    Lon = table.Column<double>(type: "float", nullable: false),
                    TzId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LocaltimeEpoch = table.Column<int>(type: "int", nullable: false),
                    Localtime = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DbUserModelId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbWeatherLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DbWeatherLocation_Users_DbUserModelId",
                        column: x => x.DbUserModelId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DbWeatherLocation_DbUserModelId",
                table: "DbWeatherLocation",
                column: "DbUserModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DbWeatherLocation");
        }
    }
}
