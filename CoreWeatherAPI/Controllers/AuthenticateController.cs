﻿using AutoMapper;
using CoreWeatherAPI.Data;
using CoreWeatherAPI.Filters;
using CoreWeatherAPI.Models;
using CoreWeatherAPI.TokenAuthentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Controllers
{
    public class AuthenticateController : Controller
    {
        readonly ITokenManager _tokenManager;
        private readonly IUserContextService _userContext;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private string _thisAPI;

        public AuthenticateController(ITokenManager tokenManager,
            IUserContextService userContext,
            IMapper mapper,
            IConfiguration configuration)
        {
            _tokenManager = tokenManager;
            _userContext = userContext;
            _mapper = mapper;
            _configuration = configuration;
            _thisAPI = _configuration.GetValue<string>("ApiHost");
        }

        [HttpPost("createuser")]
        [AllowAnonymous]
        public async Task<IActionResult> CreateUser([FromBody] UserModel user)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var mappedData = _mapper.Map<DbUserModel>(user);
            var result = await _userContext.CreateUser(mappedData);
            return Created($"{_thisAPI}createuser",result);
        }

        [HttpPost("login")]
        [TokenAuthenticationFilter]
        public IActionResult Login([FromBody] LoginRequest loginRequest)
        {
            if (!ModelState.IsValid)
                return null;

            var user = _userContext.Login(loginRequest.Email);
            return Ok(user);
        }

        [HttpPost("getuser")]
        [AllowAnonymous]
        public IActionResult GetUserForRegistration([FromBody] string email)
        {
            var user = _userContext.GetUser(email);
            return Ok(user);
        }

        [HttpPost("resetpassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([FromBody] string email)
        {
            var reset = await _userContext.SendResetEmail(email);
            return Ok(reset);
        }

        [HttpPost("authenticateuser")]
        [AllowAnonymous]
        public IActionResult Authenticate([FromBody] LoginRequest loginRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (_tokenManager.Authenticate(loginRequest.Email, loginRequest.Password))
            {
                var newToken = _tokenManager.NewToken(loginRequest.Email);
                return Ok(newToken);
            }
            else
            {
                ModelState.AddModelError("Unauthorized", "You are not authorized");
                return Unauthorized(ModelState);
            }
        }
    }
}
