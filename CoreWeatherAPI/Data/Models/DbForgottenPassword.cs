﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Data
{
    public class DbForgottenPassword
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpireyDate { get; set; }
        public bool IsFulfilled { get; set; }
    }
}
