﻿using AutoMapper;
using CoreWeatherAPI.Data;
using CoreWeatherAPI.Filters;
using CoreWeatherAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Controllers
{
    public class WeatherController : ControllerBase
    {

        private readonly IWeatherContextService _weatherContext;

        public WeatherController(IWeatherContextService weatherContext)
        {
            _weatherContext = weatherContext;
        }

        /// <summary>
        /// Api endpoint for loading weather by search
        /// Currently have not added any validation
        /// </summary>
        /// <param name="q">Location</param>
        /// <returns></returns>
        [HttpGet("current/{q}")]
        [AllowAnonymous]
        public async Task<IActionResult> LoadWeatherBySearch(string q)
        {
            var data = await _weatherContext.LoadWeatherBySearch(q);
            return Ok(data);
        }

        /// <summary>
        /// Api endpoint for loading List of locations 
        /// Currently have not added any validation
        /// </summary>
        /// <param name="q">Location</param>
        /// <returns></returns>
        [HttpGet("search/{q}")]
        [AllowAnonymous]
        public async Task<IActionResult> LoadLocationsBySearch(string q)
        {
            var data = await _weatherContext.LoadLocationsBySearch(q);
            return Ok(data);
        }

        [HttpGet("forecast/{q}")]
        [AllowAnonymous]
        public async Task<IActionResult> LoadForcastByLocation(string q)
        {
            var data = await _weatherContext.LoadForecastByLocation(q);
            return Ok(data);
        }
    }
}
