﻿namespace CoreWeatherAPI.Models
{
    public class WeatherCondition
    {
        public string Text { get; set; }

        public string Icon { get; set; }

        public int Code { get; set; }
    }

}
