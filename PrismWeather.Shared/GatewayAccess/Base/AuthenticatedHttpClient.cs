﻿using Prism.Ioc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using WeatherApp.Shared.Authentication;

namespace WeatherApp.Shared.GatewayAccess.Base
{
    public class AuthenticatedHttpClient : HttpClientHandler
    {
        public AuthenticatedHttpClient()
        {

        }

        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var auth = ContainerLocator.Container.Resolve<IAuthenticateManager>();

            if (!string.IsNullOrEmpty(auth.Token))
            {
                var authValue = new AuthenticationHeaderValue("Bearer", auth.Token);
                request.Headers.Authorization = authValue;
            }

            var result = await base.SendAsync(request, new CancellationTokenSource().Token);
            return result;
        }
    }
}
