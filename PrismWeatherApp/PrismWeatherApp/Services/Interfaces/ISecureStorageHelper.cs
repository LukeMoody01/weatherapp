﻿using System.Threading.Tasks;

namespace WeatherApp.Services
{
    public interface ISecureStorageHelper
    {
        Task<string> GetValue(string key, string defaultValue = null);

        Task SetValue(string key, string value);

        bool DeleteKey(string key);
        void DeleteAllKey();
    }
}