﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherApp.Shared.Models
{
    public class ForecastDayModel
    {
        private string _date;
        public string date
        {
            get 
            {
                DateTime.TryParse(_date, out var result);
                _date = result.DayOfWeek.ToString();
                if (_date == DateTime.Now.DayOfWeek.ToString())
                    _date = "Today";
                else if (_date == DateTime.Now.AddDays(1).DayOfWeek.ToString())
                    _date = "Tomorrow";
                return _date;
            }
            set {  _date = value; }
        }
        public int date_epoch { get; set; }
        public DayModel day { get; set; }
        public AstroModel astro { get; set; }
        public List<HourModel> hour { get; set; }
    }
}
