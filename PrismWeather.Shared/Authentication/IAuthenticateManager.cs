﻿namespace WeatherApp.Shared.Authentication
{
    public interface IAuthenticateManager
    {
        string Token { get; }

        void ForceSetToken(string token);
    }
}