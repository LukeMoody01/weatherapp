﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherApp.Helpers
{
    public interface INotification
    {
        void CreateNotification(string title, string body);
    }
}
