﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreWeatherAPI.Migrations
{
    public partial class UpdateNaming : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PId",
                table: "UserFavouritedLocations",
                newName: "PrimaryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PrimaryId",
                table: "UserFavouritedLocations",
                newName: "PId");
        }
    }
}
