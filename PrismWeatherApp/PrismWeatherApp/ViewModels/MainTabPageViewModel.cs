﻿using Prism.Navigation;

namespace PrismWeatherApp.ViewModels
{
    public class MainTabPageViewModel : ViewModelBase
    {
        public MainTabPageViewModel(IBaseService baseService) : base(baseService)
        {
        }
    }
}
