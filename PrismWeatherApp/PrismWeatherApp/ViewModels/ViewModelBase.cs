﻿using Prism.AppModel;
using Prism.Commands;
using Prism.Ioc;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Windows.Input;
using WeatherApp.Services;

namespace PrismWeatherApp.ViewModels
{
    public interface IBaseService
    {
        INavigationService NavigationService { get; }
        IPopupDialog PopupDialog { get; }
    }

    public class BaseService : IBaseService
    {
        public INavigationService NavigationService { get; }

        public IPopupDialog PopupDialog { get; }

        public BaseService(INavigationService navigationService,
            IPopupDialog popupDialog)
        {
            NavigationService = navigationService;
            PopupDialog = popupDialog;
        }
    }


    public class ViewModelBase : BindableBase, IInitializeAsync, INavigationAware, IDestructible, IPageLifecycleAware
    {
        public INavigationService _navigationService;
        public IPopupDialog _popupDialog;
        private bool _isBusy;
        private bool _isGettingWeather;
        private DateTime _lastClickDebouncer;

        public DateTime LastClickDebouncer
        {
            get { return _lastClickDebouncer; }
            set { SetProperty(ref _lastClickDebouncer, value); }
        }

        public ICommand GoBackCommand { get; }
        
        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        public bool IsGettingWeather
        {
            get => _isGettingWeather;
            set => SetProperty(ref _isGettingWeather, value);
        }

        public ViewModelBase(IBaseService baseService)
        {
            _navigationService = baseService.NavigationService;
            _popupDialog = baseService.PopupDialog;  
            GoBackCommand = new DelegateCommand(async () => await GoBack());
        }

        public virtual async Task GoBack()
        {
            if (!CanClick(1000))
                return;
            await _navigationService.GoBackAsync();
        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {

        }

        public virtual void Destroy()
        {

        }

        public void OnAppearing()
        {
        }

        public void OnDisappearing()
        {
        }

        public bool CanClick(double delay)
        {
            if (LastClickDebouncer.AddMilliseconds(delay) >= DateTime.Now)
                return false;
            LastClickDebouncer = DateTime.Now;
            return true;
        }

        public bool IsValidEmail(string email)
        {
            try
            {
                var addr = new EmailAddressAttribute().IsValid(email);
                return addr;
            }
            catch
            {
                return false;
            }
        }

        public async Task ShowPopup(string popupAnimationType, string popupHeader, string popupBody)
        {
            await _popupDialog.ShowPopup(popupAnimationType, popupHeader, popupBody);
        }

        public virtual Task InitializeAsync(INavigationParameters parameters)
        {
            return Task.CompletedTask;
        }
    }
}
