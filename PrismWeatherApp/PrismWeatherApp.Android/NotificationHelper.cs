﻿using Android.App;
using Android.Content;
using Android.Media;
using AndroidX.Core.App;
using Prism.Ioc;
using PrismWeatherApp.Droid;
using System;
using System.Collections.Generic;
using System.Linq;
using WeatherApp.Helpers;
using WeatherApp.Services;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Services.Interfaces;
using Xamarin.Forms;

[assembly:Dependency(typeof(NotificationHelper))]
namespace PrismWeatherApp.Droid
{
    public class NotificationHelper : INotification
    {
        private Context _context; // Activity
        private NotificationManager _notificationManager; 
        private NotificationCompat.Builder _builder;
        private static string NOTIFICATION_CHANNEL_ID = "10023"; // Seperate catorgories

        public NotificationHelper()
        {
            _context = global::Android.App.Application.Context;
        }

        [Obsolete]
        public void CreateNotification(string title, string body)
        {
            try
            {
                var intent = new Intent(_context, typeof(MainActivity));
                intent.AddFlags(ActivityFlags.ClearTop); // When we use this flag it will reset the activity
                intent.PutExtra(title, body);
                var pendingIntent = PendingIntent.GetActivity(_context, 0, intent, PendingIntentFlags.OneShot);

                var sound = global::Android.Net.Uri.Parse(ContentResolver.SchemeAndroidResource + "://" + _context.PackageName + "/" + Resource.Raw.Notification);
                var alarmAttributes = new AudioAttributes.Builder()
                    .SetContentType(AudioContentType.Sonification)
                    .SetUsage(AudioUsageKind.Notification).Build();

                _builder = new NotificationCompat.Builder(_context);
                _builder.SetSmallIcon(Resource.Drawable.notificationIcon);
                _builder.SetContentTitle(title)
                    .SetSound(sound)
                    .SetAutoCancel(true)
                    .SetContentTitle(title)
                    .SetContentText(body)
                    .SetChannelId(NOTIFICATION_CHANNEL_ID)
                    .SetPriority((int)NotificationPriority.High)
                    .SetVibrate(new long[0])
                    .SetDefaults((int)NotificationDefaults.Sound | (int)NotificationDefaults.Vibrate)
                    .SetVisibility((int)NotificationVisibility.Public)
                    .SetSmallIcon(Resource.Drawable.notificationIcon)
                    .SetContentIntent(pendingIntent);

                _notificationManager = _context.GetSystemService(Context.NotificationService) as NotificationManager;

                if (global::Android.OS.Build.VERSION.SdkInt >= global::Android.OS.BuildVersionCodes.O)
                {
                    NotificationImportance importance = global::Android.App.NotificationImportance.High;

                    NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, title, importance);
                    notificationChannel.EnableLights(true);
                    notificationChannel.EnableVibration(true);
                    notificationChannel.SetSound(sound, alarmAttributes);
                    notificationChannel.SetShowBadge(true);
                    notificationChannel.Importance = NotificationImportance.High;
                    notificationChannel.SetVibrationPattern(new long[] { 100, 200, 300, 400, 500, 400, 300, 200, 400 });

                    if (_notificationManager != null)
                    {
                        _builder.SetChannelId(NOTIFICATION_CHANNEL_ID);
                        _notificationManager.CreateNotificationChannel(notificationChannel);
                    }
                }

                _notificationManager.Notify(0, _builder.Build());
            }
            catch (Exception)
            {
            }
        }

        [Obsolete]
        public async void CreateAlertForFavourite(string title, string body)
        {
            try
            {
                var appUserService = ContainerLocator.Container.Resolve<IAppUserService>();
                var secureStorage = ContainerLocator.Container.Resolve<ISecureStorageHelper>();
                var weatherService = ContainerLocator.Container.Resolve<IWeatherService>();

                var userId = await secureStorage.GetValue("user");
                if (userId == null)
                    return;

                IEnumerable<SearchedLocationModel> result = await weatherService.GetFavouritesFromCache(userId.ToString());
                List<SearchedLocationModel> favouritesList = result.ToList();

                if (favouritesList.Count == 0)
                    return;

                var random = new Random();
                int index = random.Next(favouritesList.Count);

                var weatherResult = await weatherService.GetWeather($"{favouritesList[index].Name}-{favouritesList[index].Region}");
                if (weatherResult == null)
                    return;

                var intent = new Intent(_context, typeof(MainActivity));
                intent.AddFlags(ActivityFlags.ClearTop); // When we use this flag it will reset the activity
                intent.PutExtra("LocationName", $"{weatherResult.Location.Name} {weatherResult.Location.Region}");
                var pendingIntent = PendingIntent.GetActivity(_context, 0, intent, PendingIntentFlags.OneShot);

                var sound = global::Android.Net.Uri.Parse(ContentResolver.SchemeAndroidResource + "://" + _context.PackageName + "/" + Resource.Raw.Notification);
                var alarmAttributes = new AudioAttributes.Builder()
                    .SetContentType(AudioContentType.Sonification)
                    .SetUsage(AudioUsageKind.Notification).Build();

                _builder = new NotificationCompat.Builder(_context);
                _builder.SetSmallIcon(Resource.Drawable.notificationIcon);
                _builder.SetContentTitle(title)
                    .SetSound(sound)
                    .SetAutoCancel(true)
                    .SetContentTitle($"Moody Weather - {weatherResult.Location.Name}")
                    .SetContentText($"The temperature today for your favourite \"{weatherResult.Location.Name}\" is {weatherResult.Current.TempC}")
                    .SetChannelId(NOTIFICATION_CHANNEL_ID)
                    .SetPriority((int)NotificationPriority.High)
                    .SetVibrate(new long[0])
                    .SetDefaults((int)NotificationDefaults.Sound | (int)NotificationDefaults.Vibrate)
                    .SetVisibility((int)NotificationVisibility.Public)
                    .SetSmallIcon(Resource.Drawable.notificationIcon)
                    .SetContentIntent(pendingIntent);

                _notificationManager = _context.GetSystemService(Context.NotificationService) as NotificationManager;

                if (global::Android.OS.Build.VERSION.SdkInt >= global::Android.OS.BuildVersionCodes.O)
                {
                    NotificationImportance importance = global::Android.App.NotificationImportance.High;

                    NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, title, importance);
                    notificationChannel.EnableLights(true);
                    notificationChannel.EnableVibration(true);
                    notificationChannel.SetSound(sound, alarmAttributes);
                    notificationChannel.SetShowBadge(true);
                    notificationChannel.Importance = NotificationImportance.High;
                    notificationChannel.SetVibrationPattern(new long[] { 100, 200, 300, 400, 500, 400, 300, 200, 400 });

                    if (_notificationManager != null)
                    {
                        _builder.SetChannelId(NOTIFICATION_CHANNEL_ID);
                        _notificationManager.CreateNotificationChannel(notificationChannel);
                    }
                }
                _notificationManager.Notify(0, _builder.Build());
            }
            catch (Exception)
            {
            }
        }
    }
}
