﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WeatherApp.Shared.Repository
{
    public interface IRepository<T> where T : class, new()
    {
        Task<List<T>> Get();
        Task<T> Get(string id);
        Task<List<T>> Get<TValue>(Expression<Func<T, bool>> predicate = null, Expression<Func<T, TValue>> orderBy = null);
        Task<T> Get(Expression<Func<T, bool>> predicate);
        AsyncTableQuery<T> AsQueryable();
        Task<int> InsertItem(T entity);
        Task<int> UpdateItem(T entity);
        Task<int> DeleteItem(T entity);
        Task<int> DeleteAllItems();
        Task<int> InsertAllItems(IEnumerable<T> list);
        Task<List<T>> Query(string query, params object[] args);
        Task<int> Execute(string sqlText);
    }
}