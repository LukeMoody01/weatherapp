﻿using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using WeatherApp.Helpers;
using WeatherApp.Shared.Authentication;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.Events;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Services.Interfaces;
using Xamarin.Essentials;

namespace PrismWeatherApp.ViewModels
{
    public class WeatherPageViewModel : ViewModelBase
    {
        private readonly IWeatherService _weatherService;
        private readonly IEventAggregator _eventAggregator;
        private readonly ICurrentUser _currentUser;
        private string _windMPH;
        private string _areaPressure;
        private string _weatherLocation;
        private string _localTime;
        private string _tempC;
        private string _humidity;
        private string _favouriteButton;
        private string _locationTime;
        private string _cloudiness;
        private string _weatherConditionText;
        private bool _isFavourite;
        private bool _hasPageLoaded;
        private ObservableCollection<ForecastDayModel> _locationForecast;

        private SearchedLocationModel _thisLocation { get; set; }
        public ObservableCollection<ForecastDayModel> LocationForecast
        {
            get { return _locationForecast; }
            set { SetProperty(ref _locationForecast, value); }
        }
        public WeatherModel LocationData { get; set; }
        public string WeatherLocation
        {
            get { return _weatherLocation; }
            set { SetProperty(ref _weatherLocation, value); }
        }
        public string LocalTime
        {
            get { return _localTime; }
            set { SetProperty(ref _localTime, value); }
        }
        public string TempC
        {
            get { return _tempC; }
            set { SetProperty(ref _tempC, value); }
        }
        public string WindKPH
        {
            get { return _windMPH; }
            set { SetProperty(ref _windMPH, value); }
        }
        public string AreaPressure
        {
            get { return _areaPressure; }
            set { SetProperty(ref _areaPressure, value); }
        }
        public string Humidity
        {
            get { return _humidity; }
            set { SetProperty(ref _humidity, value); }
        }
        public string Cloudiness
        {
            get { return _cloudiness; }
            set { SetProperty(ref _cloudiness, value); }
        }
        public string WeatherConditionText
        {
            get { return _weatherConditionText; }
            set { SetProperty(ref _weatherConditionText, value); }
        }
        public string LocationTime
        {
            get { return _locationTime; }
            set { SetProperty(ref _locationTime, value); }
        }
        public string FavouriteButton
        {
            get
            {
                if (IsFavourite)
                    _favouriteButton = "Unsave As Favourite";
                else
                    _favouriteButton = "Save As Favourite";
                return _favouriteButton;
            }
            set
            {
                SetProperty(ref _favouriteButton, value);
                RaisePropertyChanged(nameof(FavouriteButton));
            }
        }
        public bool IsFavourite
        {
            get { return _isFavourite; }
            set
            {
                SetProperty(ref _isFavourite, value);
                RaisePropertyChanged(nameof(FavouriteButton));
            }
        }
        public bool HasPageLoaded
        {
            get { return _hasPageLoaded; }
            set { SetProperty(ref _hasPageLoaded, value); }
        }

        public ICommand UpdateFavouriteCommand { get; }
        public ICommand ShareWeatherCommand { get; }

        public WeatherPageViewModel(IBaseService baseService,
            IWeatherService weatherService,
            IEventAggregator eventAggregator,
            ICurrentUser appUser) : base(baseService)
        {
            _weatherService = weatherService;
            _eventAggregator = eventAggregator;
            _currentUser = appUser;
            UpdateFavouriteCommand = new DelegateCommand(async () => await UpdateFavourite());
            ShareWeatherCommand = new DelegateCommand(async () => await ShareWeather());
        }

        // Commented out temporarily, uncomment when running unit tests
        //public override async Task InitializeAsync(INavigationParameters parameters) 
        //{
        //    OnNavigatedTo(parameters);
        //}

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            HasPageLoaded = false;
            IsGettingWeather = true;
            _thisLocation = parameters.GetValue<SearchedLocationModel>("SelectedLocaton");
            var locationName = parameters.GetValue<string>("LocationData");

            await PopulateDataAsync(locationName);
            await GetLocationAsync();

            IsGettingWeather = false;
            HasPageLoaded = true;
        }

        private async Task ShareWeather()
        {
            var location = LocationData.Location.Name.Replace(" ", "-");
            var state = LocationData.Location.Region.Replace(" ", "-");
            var link = location + "-" + state;
            await Share.RequestAsync(new ShareTextRequest
            {
                Text = $@"http://moodyweather?LocationName={link}",
                Title = "Share This Weather!"
            });
        }

        private async Task UpdateFavourite()
        {
            if (!CanClick(2000))
                return;

            IsBusy = true;
            if (IsFavourite)
            {
                _thisLocation.UserFavourited = _currentUser.person.Id;
                var isDeleted = await _weatherService.DeleteFavourite(_thisLocation);
                if (isDeleted)
                {
                    IsFavourite = false;
                    await ShowPopup(AppConstants.SUCCESSANIMATION, AppConstants.SUCCESSHEADER, $"{WeatherLocation} {AppConstants.SUCCESSREMOVELOCATIONFAVOURITE}");
                }
                else
                {
                    await ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORHEADER, $"{WeatherLocation} {AppConstants.ERRORREMOVELOCATIONFAVOURITE}");
                }
            }
            else
            {
                _thisLocation.PrimaryId = Guid.NewGuid().ToString();
                if (_thisLocation.Name.Contains(","))
                {
                    _thisLocation.Name = LocationNameHelper.GetUntilOrEmpty(_thisLocation.Name);
                }
                _thisLocation.UserFavourited = _currentUser.person.Id;
                var isCreated = await _weatherService.CreateFavourite(_thisLocation);
                if (isCreated)
                {
                    IsFavourite = true;
                    await ShowPopup(AppConstants.FAVOURITEANIMATION, AppConstants.FAVOURITEHEADER, $"{WeatherLocation} {AppConstants.SUCCESSLOCATIONFAVOURITE}");
                }
                else
                {
                    await ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORHEADER, $"{WeatherLocation} {AppConstants.ERRORLOCATIONFAVOURITE}");
                }
            }
            IsBusy = false;
            _eventAggregator.GetEvent<FavouriteChangedEvent>().Publish();
        }

        private async Task GetLocationAsync()
        {
            if (_thisLocation == null)
            {
                _thisLocation = GetFavourite();

                if (_thisLocation == null)
                {
                    _thisLocation = await FindLocationAsync($"{LocationData.Location.Lat},{LocationData.Location.Lon}");
                    IsFavourite = false;
                    return;
                }
                IsFavourite = true;
            }
            else
            {
                IsFavourite = _currentUser.favourites.Exists(x => x.Id == _thisLocation.Id);
            }
        }

        private async Task PopulateDataAsync(string locationName)
        {
            LocationData = await _weatherService.GetForecast(locationName);
            if (LocationData.Current == null)
                return;

            WeatherLocation = LocationData.Location.Name;
            LocalTime = LocationData.Location.Localtime;
            TempC = $"{LocationData.Current.TempC}";
            WindKPH = $"{LocationData.Current.WindKph} kph";
            AreaPressure = $"{LocationData.Current.PressureMb} hpa";
            Humidity = $"{LocationData.Current.Humidity}%";
            Cloudiness = $"{LocationData.Current.Cloud}%";
            WeatherConditionText = LocationData.Current.Condition.Text;

            DateTime.TryParse(LocationData.Location.Localtime, out var result);
            LocationTime = $"{result.ToString("M")}, {result.ToString("t")}";
            LocationForecast = new ObservableCollection<ForecastDayModel>(LocationData.Forecast.Forecastday);
        }

        private async Task<SearchedLocationModel> FindLocationAsync(string locationCords)
        {
            var locations = await _weatherService.GetSearchedLocatons(locationCords);
            return locations.Find(x => x.Name.Contains(LocationData.Location.Name));
        }

        private SearchedLocationModel GetFavourite()
        {
            return _currentUser.favourites.Find(x => x.Name.Contains(LocationData.Location.Name));
        }

    }
}
