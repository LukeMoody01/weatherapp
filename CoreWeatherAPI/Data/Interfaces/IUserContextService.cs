﻿using CoreWeatherAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Data
{
    public interface IUserContextService
    {
        Task<bool> CreateUser(DbUserModel user);
        DbUserModel Login(string email);
        Task<bool> CreateFavourite(DbSearchedLocation favourite);
        IEnumerable<DbSearchedLocation> GetFavourites(string userId);
        Task<bool> DeleteFavourite(DbSearchedLocation favourite);
        Task<bool> ResetPassword(string passWord, string userId);
        DbUserModel GetUser(string email);
        Task<bool> SendResetEmail(string userEmail);
        DbUserModel GetUserById(string userID);
    }
}