﻿using Polly;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WeatherApp.Shared.Repository.Interfaces;

namespace WeatherApp.Shared.Repository
{
    public class Repository<T> : IRepository<T> where T : class, new()
    {
        private Lazy<IDBConnection> _lazyDBConnection;

        public Repository(Lazy<IDBConnection> lazyDBConnection)
        {
            _lazyDBConnection = lazyDBConnection;
        }

        protected IDBConnection _dbConnection => _lazyDBConnection.Value;


        // Queries the database and returns the results as an array.
        public AsyncTableQuery<T> AsQueryable()
        {
            return AttemptAndRetry(() => _dbConnection.GetAsyncConnection().Table<T>());
        }

        // Deletes an object from the database
        public async Task<int> DeleteItem(T entity)
        {
            return await AttemptAndRetry(() => _dbConnection.GetAsyncConnection().DeleteAsync(entity)).ConfigureAwait(false);
        }

        // Deletes all object's from the database
        public async Task<int> DeleteAllItems()
        {
            return await AttemptAndRetry(() => _dbConnection.GetAsyncConnection().DeleteAllAsync<T>()).ConfigureAwait(false);
        }

        // INSERTS UPDATES AND DELETES
        public async Task<int> Execute(string sqlText)
        {
            return await AttemptAndRetry(() => _dbConnection.GetAsyncConnection().ExecuteAsync(sqlText)).ConfigureAwait(false);
        }

        // Gets the list of items from the db
        public async Task<List<T>> Get()
        {
            return await AttemptAndRetry(() => _dbConnection.GetAsyncConnection().Table<T>().ToListAsync()).ConfigureAwait(false);
        }

        // Gets the list of objects from the db via linq expression
        public async Task<List<T>> Get<TValue>(Expression<Func<T, bool>> predicate = null, Expression<Func<T, TValue>> orderBy = null)
        {
            return await AttemptAndRetry(async () =>
            {
                var query = _dbConnection.GetAsyncConnection().Table<T>();

                if (predicate != null)
                    query = query.Where(predicate);

                if (orderBy != null)
                    query = query.OrderBy(orderBy);

                return await query.ToListAsync();
            }).ConfigureAwait(false);
        }

        // Get object by primary key
        public async Task<T> Get(string id)
        {
            return await AttemptAndRetry(() => _dbConnection.GetAsyncConnection().FindAsync<T>(id)).ConfigureAwait(false);
        }

        // Get's the first object by linq expression
        public async Task<T> Get(Expression<Func<T, bool>> predicate)
        {
            return await AttemptAndRetry(() => _dbConnection.GetAsyncConnection().FindAsync<T>(predicate)).ConfigureAwait(false);
        }

        // Inserts the given object and auto increments the key
        public async Task<int> InsertItem(T entity)
        {
            return await AttemptAndRetry(() => _dbConnection.GetAsyncConnection().InsertAsync(entity)).ConfigureAwait(false);
        }

        // Inserts all given object's 
        public async Task<int> InsertAllItems(IEnumerable<T> list)
        {
            return await AttemptAndRetry(() => _dbConnection.GetAsyncConnection().InsertAllAsync(list)).ConfigureAwait(false);
        }

        // Returns A list with one result for each row returned by the query.
        public async Task<List<T>> Query(string query, params object[] args)
        {
            return await AttemptAndRetry(() => _dbConnection.GetAsyncConnection().QueryAsync<T>(query, args)).ConfigureAwait(false);
        }

        // Update all the columsn of a table using the object. Primary key is required
        public async Task<int> UpdateItem(T entity)
        {
            return await AttemptAndRetry(() => _dbConnection.GetAsyncConnection().UpdateAsync(entity)).ConfigureAwait(false);
        }

        // Update all object's
        public async Task<int> UpdateAllItems(IEnumerable<T> entities)
        {
            return await AttemptAndRetry(() => _dbConnection.GetAsyncConnection().UpdateAllAsync(entities)).ConfigureAwait(false);
        }

        protected Task<TResult> AttemptAndRetry<TResult>(Func<Task<TResult>> action, int numRetries = 3)
        {
            return Policy.Handle<SQLiteException>().WaitAndRetryAsync(numRetries, PollyRetryAttempt).ExecuteAsync(action);
        }

        protected TResult AttemptAndRetry<TResult>(Func<TResult> action, int numRetries = 3)
        {
            return Policy.Handle<SQLiteException>().WaitAndRetry(numRetries, PollyRetryAttempt).Execute(action);
        }

        private TimeSpan PollyRetryAttempt(int attemptNumber) => TimeSpan.FromMilliseconds(Math.Pow(2, attemptNumber));
    }
}
