﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherApp.Shared.Models;

namespace WeatherApp.Shared.Repository.Interfaces
{
    public interface IWeatherRepository
    {
        Task<IEnumerable<SearchedLocationModel>> GetFavourites(string userId);
        Task<bool> SaveFavourites(IEnumerable<SearchedLocationModel> favourites);
        Task<bool> SaveFavourite(SearchedLocationModel favourite);
        Task<bool> RemoveFavourite(SearchedLocationModel favourite);
    }
}