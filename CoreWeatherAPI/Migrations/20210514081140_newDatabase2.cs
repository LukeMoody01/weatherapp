﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoreWeatherAPI.Migrations
{
    public partial class newDatabase2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DbWeatherLocation");

            migrationBuilder.DropTable(
                name: "FavouritedLocations");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
