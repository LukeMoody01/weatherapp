﻿using Nito.AsyncEx;
using SQLite;
using System;
using System.IO;
using System.Threading.Tasks;
using WeatherApp.Shared.Repository.Interfaces;

namespace WeatherApp.Shared.Repository.Implementations
{
    public class DBConnection : IDBConnection
    {
        private string _dbPath;
        private SQLiteAsyncConnection _dbConnection;
        private static readonly AsyncLock _mutex = new AsyncLock();

        public DBConnection()
        {
        }

        public SQLiteAsyncConnection GetAsyncConnection()
        {
            return _dbConnection;
        }

        public void SetDBPath(string dbPath)
        {
            _dbPath = dbPath;
        }

        public async Task CloseConnection()
        {
            using (await _mutex.LockAsync())
            {
                if (_dbConnection == null)
                {
                    return;
                }

                await Task.Factory.StartNew(async () =>
                {
                    await _dbConnection.CloseAsync();

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                });
            }
        }

        public void CreateDBConnection()
        {
            var connectionString = new SQLiteConnectionString(_dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.SharedCache, true);
            _dbConnection = new SQLiteAsyncConnection(connectionString);
        }

        public async Task DeleteDatabaseFile()
        {
            var fi = new FileInfo(_dbPath);

            while (fi.Exists)
            {
                bool successful = false;
                try
                {
                    if (!successful)
                        fi.Delete();

                    successful = true;
                }
                catch
                {
                    await _dbConnection?.CloseAsync();
                }

                fi.Refresh();
                System.Threading.Thread.Sleep(100);
            }

            if (_dbConnection != null)
                await _dbConnection?.CloseAsync();

            _dbConnection = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
