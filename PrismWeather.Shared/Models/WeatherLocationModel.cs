﻿using Newtonsoft.Json;
using System;

namespace WeatherApp.Shared.Models
{
    public class WeatherLocationModel
    {
        private string _localtime;

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("lon")]
        public double Lon { get; set; }

        [JsonProperty("tzid")]
        public string TzId { get; set; }

        [JsonProperty("localtimeepoch")]
        public int LocaltimeEpoch { get; set; }

        [JsonProperty("localtime")]
        public string Localtime
        {
            get 
            {
                return _localtime; 
            }
            set { _localtime = value; }
        }
    }
}