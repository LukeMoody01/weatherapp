﻿using PrismWeatherApp.iOS.CustomRenderer;
using PrismWeatherApp.Views.Controls;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(SearchFrame), typeof(SearchFrameRenderer))]
namespace PrismWeatherApp.iOS.CustomRenderer
{
    class SearchFrameRenderer : FrameRenderer
    {
        public SearchFrameRenderer()
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            if (Element != null)
            {
                base.OnElementChanged(e);

                Element.Padding = 10;
            }
        }
    }
}