﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Models
{
    public class Astro
    {
        public string Sunrise { get; set; }

        public string Sunset { get; set; }

        public string Moonrise { get; set; }

        public string Moonset { get; set; }

        public string MoonPhase { get; set; }

        public string MoonIllumination { get; set; }
    }
}
