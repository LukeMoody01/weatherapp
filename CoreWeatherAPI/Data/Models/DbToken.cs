﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoreWeatherAPI.Data
{
    public class DbToken
    {
        [Key]
        public string UniqueID { get; set; }
        public string UserId { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
