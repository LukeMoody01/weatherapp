﻿using Newtonsoft.Json;
using SQLite;

namespace WeatherApp.Shared.Models
{
    public class SearchedLocationModel
    {
        [JsonProperty("PrimaryId")]
        [PrimaryKey]
        public string PrimaryId { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("lon")]
        public double Lon { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("userFavourited")]
        public string UserFavourited { get; set; }
    }
}
