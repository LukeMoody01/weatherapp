﻿using System.Runtime.Remoting.Contexts;
using PrismWeatherApp.iOS.CustomRenderer;
using PrismWeatherApp.Views.Controls;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BorderlessEntry), typeof(BorderlessEntryRenderer))]
namespace PrismWeatherApp.iOS.CustomRenderer
{
    public class BorderlessEntryRenderer : EntryRenderer
    {
        public BorderlessEntryRenderer()
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                base.OnElementChanged(e);

                Control.BorderStyle = UITextBorderStyle.None;
            }
        }
    }
}