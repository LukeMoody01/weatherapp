﻿namespace WeatherApp.Shared.BarrackShared
{
    public interface IAppSettingsProvider
    {
        string GetRestBaseApi();
    }
}
