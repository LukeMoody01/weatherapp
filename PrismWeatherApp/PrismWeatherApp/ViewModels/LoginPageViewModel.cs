﻿using Prism.Commands;
using Prism.Ioc;
using Prism.Navigation;
using Prism.Services;
using PrismWeatherApp.Views.Popups;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using WeatherApp.Services;
using WeatherApp.Shared.Authentication;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Repository.Interfaces;
using WeatherApp.Shared.Services.Interfaces;

namespace PrismWeatherApp.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        #region Fields
        private readonly IAuthenticateService _authenticateService;
        #endregion

        #region Properties
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        #endregion

        #region Commands
        public ICommand LoginCommand { get; set; }
        public ICommand ForgotPasswordCommand { get; set; }
        public ICommand SignUpCommand { get; set; }
        #endregion

        #region Ctor
        public LoginPageViewModel(IBaseService baseService,
            IAuthenticateService authenticateService) : base(baseService)
        {
            _authenticateService = authenticateService;
            LoginCommand = new DelegateCommand(async () => await LoginToApp());
            ForgotPasswordCommand = new DelegateCommand(async () => await ForgotPasswordNavigation());
            SignUpCommand = new DelegateCommand(async () => await SignUpNavigation());
        }
        #endregion

        #region Methods
        private async Task SignUpNavigation()
        {
            if (!CanClick(1000))
                return;

            await _navigationService.NavigateAsync("/RegisterPage");
        }

        private async Task ForgotPasswordNavigation()
        {
            if (!CanClick(1000))
                return;

            await _navigationService.NavigateAsync("ForgottenPasswordPage");
        }

        private async Task LoginToApp()
        {
            if (!CanClick(3000))
                return;
            IsBusy = true;

            if (!string.IsNullOrWhiteSpace(EmailAddress) && !string.IsNullOrWhiteSpace(Password))
            {
                var request = new LoginRequestModel
                {
                    Password = Password,
                    Email = EmailAddress.ToLower()
                };

                if (await _authenticateService.Login(request))
                {
                    await _navigationService.NavigateAsync("/AutoLoginPage");
                    IsBusy = false;
                    return;
                }
            }
            await ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORHEADER, AppConstants.ERRORINCORRECTINFO);
            IsBusy = false;
        }
        #endregion
    }
}
