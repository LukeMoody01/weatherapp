﻿using Prism.Commands;
using Prism.Navigation;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Services.Interfaces;

namespace PrismWeatherApp.ViewModels
{
    public class RegisterPageViewModel : ViewModelBase
    {
        #region Fields
        private readonly IAppUserService _appUserService;
        #endregion

        #region Commands
        public ICommand RegisterCommand { get; set; }
        public ICommand SignInCommand { get; set; }
        #endregion

        #region Properties
        public string Username { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string ConfirmedPassword { get; set; }
        #endregion

        #region Ctor
        public RegisterPageViewModel(IBaseService baseService,
            IAppUserService appUserService) : base(baseService)
        {
            _appUserService = appUserService;
            RegisterCommand = new DelegateCommand(async () => await RegisterUser());
            SignInCommand = new DelegateCommand(async () => await SignInNavigation());
        }
        #endregion

        #region Methods
        private async Task RegisterUser()
        {
            if (!CanClick(2000))
                return;

            IsBusy = true;

            if (!await ValidateRegisterRequest())
                return;



            var hasExistingUser = await _appUserService.GetExistingUser(EmailAddress);
            if (hasExistingUser == null)
            {
                var newUser = new UserModel
                {
                    Id = Guid.NewGuid().ToString(),
                    Username = Username,
                    EmailAddress = EmailAddress,
                    Password = Password
                };

                var isCreated = await _appUserService.CreateUser(newUser);
                if (isCreated)
                {
                    await ShowPopup(AppConstants.SUCCESSANIMATION, AppConstants.SUCCESSHEADER, AppConstants.SUCCESSREGISTER);
                }
                else
                {
                    await ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORHEADER, AppConstants.ERRORREGISTER);
                }
            }
            else
            {
                await ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORHEADER, AppConstants.ERROREMAILTAKEN);
            }
            IsBusy = false;
        }

        private async Task<bool> ValidateRegisterRequest()
        {
            var validEmail = IsValidEmail(EmailAddress);

            if (Password != ConfirmedPassword
                || string.IsNullOrWhiteSpace(Username)
                || string.IsNullOrWhiteSpace(Password)
                || validEmail == false)
            {
                await ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORHEADER, AppConstants.ERRORINVALIDINFO);
                IsBusy = false;
                return false;
            }
            EmailAddress = EmailAddress.ToLower();
            return true;
        }

        private async Task SignInNavigation()
        {
            if (!CanClick(1000))
                return;

            await _navigationService.NavigateAsync("/LoginPage");
        }
        #endregion
    }
}
