﻿using SQLite;
using System.Threading.Tasks;

namespace WeatherApp.Shared.Repository.Interfaces
{
    public interface IDBConnection
    {
        SQLiteAsyncConnection GetAsyncConnection();
        void CreateDBConnection();
        Task DeleteDatabaseFile();
        Task CloseConnection();
        void SetDBPath(string dbPath);
    }
}
