﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWeatherAPI.Models
{
    public class Forecast
    {
        public List<ForecastDay> Forecastday { get; set; }
    }
}
