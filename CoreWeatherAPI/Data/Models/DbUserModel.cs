﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CoreWeatherAPI.Data
{
    public class DbUserModel
    {
        [Key]
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("EmailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }
    }
}
