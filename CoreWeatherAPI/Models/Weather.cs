﻿using System.ComponentModel.DataAnnotations;

namespace CoreWeatherAPI.Models
{
    public class Weather
    {
        [Required]
        public WeatherLocation Location { get; set; }
        public CurrentWeather Current { get; set; }
        public Forecast Forecast { get; set; }
    }
}
