﻿using SQLite;
using System;
using WeatherApp.Shared.Repository.Interfaces;

namespace WeatherApp.Shared.Repository.Implementations
{
    public class StandardDBConnection : IStandardDBConnection
    {
        private string _dbPath;
        private Lazy<SQLiteAsyncConnection> _lazySQLiteConnetion;
        private SQLiteAsyncConnection _dbConnection => _lazySQLiteConnetion.Value;


        public StandardDBConnection()
        {

        }

        public void SetDBPath(string dbPath)
        {
            _dbPath = dbPath;
        }

        public void CreateDB()
        {
            var connectionString = new SQLiteConnectionString(_dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.SharedCache, true);
            _lazySQLiteConnetion = new Lazy<SQLiteAsyncConnection>(() => new SQLiteAsyncConnection(connectionString));
        }

        public SQLiteAsyncConnection GetAsyncConnection()
        {
            return _dbConnection;
        }
    }
}
