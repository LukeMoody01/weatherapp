﻿using Prism.Navigation;
using System.Linq;
using System.Threading.Tasks;
using WeatherApp.Services;
using WeatherApp.Shared.Authentication;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.Services.Interfaces;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PrismWeatherApp.ViewModels
{
    public class AutoLoginPageViewModel : ViewModelBase
    {
        #region Fields
        private readonly ISecureStorageHelper _secureStorage;
        private readonly IAuthenticateService _authenticateService;
        private readonly IAutoLoginService _autoLoginService;
        #endregion

        #region Ctor
        public AutoLoginPageViewModel(IBaseService baseService,
            ISecureStorageHelper secureStorage,            
            IAuthenticateService authenticateService,
            IAutoLoginService autoLoginService) : base(baseService)
        {
            _secureStorage = secureStorage;
            _authenticateService = authenticateService;
            _autoLoginService = autoLoginService;
        }
        #endregion

        #region Methods
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            IsBusy = true;

            if (await _authenticateService.IsUserLoggedIn())
            {
                var userId = await _secureStorage.GetValue("user");
                await _autoLoginService.GetUserData(userId);

                var fromIntent = parameters.GetValue<bool>("FromIntent");
                if (fromIntent)
                {
                    await NavigateFromIntent();
                    return;
                }
                await NavigateToCurrentLocation();
            }
            else
            {
                await _navigationService.NavigateAsync("/LoginPage");
            }
            IsBusy = false;
        }

        private async Task NavigateFromIntent()
        {
            var navP = new NavigationParameters();
            navP.Add("LocationData", Preferences.Get("LocationData", "Sydney"));
            navP.Add("SharedLinkData", Preferences.Get("SharedLinkData", false));
            navP.Add("FromNotification", Preferences.Get("FromNotification", false));
            await _navigationService.NavigateAsync($"/MainTabPage?{KnownNavigationParameters.SelectedTab}=WeatherPage", navP);
        }

        private async Task NavigateToCurrentLocation()
        {
            try
            {
                var location = await Geolocation.GetLocationAsync();
                var placemarks = await Geocoding.GetPlacemarksAsync(location.Latitude, location.Longitude);
                var placemark = placemarks?.FirstOrDefault();
                var navP = new NavigationParameters();
                navP.Add("LocationData", placemark.Locality + "-" + placemark.AdminArea);
                navP.Add("FromAutoLogin", true);

                await _navigationService.NavigateAsync($"/MainTabPage?{KnownNavigationParameters.SelectedTab}=WeatherPage", navP);
            }
            catch (PermissionException)
            {
                await ShowPopup(AppConstants.ERRORANIMATION, AppConstants.ERRORHEADER, AppConstants.ERRORNOACCESSTOLOCATION);
                var closer = DependencyService.Get<ICloseApplication>();
                closer?.closeApplication();
            }
        }
        #endregion
    }
}
