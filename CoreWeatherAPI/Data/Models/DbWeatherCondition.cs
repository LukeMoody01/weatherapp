﻿using Newtonsoft.Json;

namespace CoreWeatherAPI.Data
{
    public class DbWeatherCondition
    {
        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }
    }

}
