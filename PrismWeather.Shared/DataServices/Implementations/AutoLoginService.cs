﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WeatherApp.Shared.Authentication;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Repository.Interfaces;
using WeatherApp.Shared.Services.Interfaces;

namespace WeatherApp.Shared.Services.Implementations
{
    public class AutoLoginService : IAutoLoginService
    {
        private readonly IAppUserService _appUserService;
        private readonly IWeatherService _weatherService;
        private readonly ICurrentUser _currentUser;
        private readonly IDBConnection _dBConnection;
        private readonly IDatabase _database;

        public AutoLoginService(IAppUserService appUserService,
            IWeatherService weatherService,
            ICurrentUser appUser,
            IDBConnection DBConnection,
            IDatabase database)
        {
            _appUserService = appUserService;
            _weatherService = weatherService;
            _currentUser = appUser;
            _dBConnection = DBConnection;
            _database = database;
        }

        public async Task GetUserData(string userId)
        {
            _dBConnection.CreateDBConnection();
            await InitializeDB();
            var userFromCache = await _appUserService.GetUserFromCache(userId);
            var favouritesFromCache = await _weatherService.GetFavouritesFromCache(userId);
            _currentUser.person = userFromCache;
            _currentUser.favourites = new List<SearchedLocationModel>(favouritesFromCache);
        }

        private async Task InitializeDB()
        {
            await _database.InitializeDatabase();
        }
    }
}
