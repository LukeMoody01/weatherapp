﻿using Android.App;
using System;
using WeatherApp.Services;
using Xamarin.Forms;

namespace PrismWeatherApp.Droid.Utility
{
    public class CloseApplication : ICloseApplication
    {
        [Obsolete]
        public void closeApplication()
        {
            var activity = (Activity)Forms.Context;
            activity.FinishAffinity();
        }
    }
}