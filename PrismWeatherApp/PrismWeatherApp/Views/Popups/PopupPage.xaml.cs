﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PrismWeatherApp.Views.Popups
{
    public partial class PopupPage : Rg.Plugins.Popup.Pages.PopupPage
    {
        private readonly string _animationType;
        private readonly string _header;
        private readonly string _body;

        public PopupPage(string errorType, string header, string body)
        {
            _animationType = errorType;
            _header = header;
            _body = body;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            animation.Animation = _animationType;
            dialogHeader.Text = _header;
            dialogMessage.Text = _body;
            base.OnAppearing();
        }

        private async void OnClose(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }

        protected override Task OnAppearingAnimationEndAsync()
        {
            return Content.FadeTo(1);
        }

        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return Content.FadeTo(1);
        }
    }
}