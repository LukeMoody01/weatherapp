﻿using SQLite;

namespace WeatherApp.Shared.Repository.Interfaces
{
    public interface IStandardDBConnection
    {
        SQLiteAsyncConnection GetAsyncConnection();
        void SetDBPath(string dbPath);
        void CreateDB();
    }
}