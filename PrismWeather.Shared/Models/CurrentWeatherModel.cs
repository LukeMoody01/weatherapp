﻿using Newtonsoft.Json;

namespace WeatherApp.Shared.Models
{
    public class CurrentWeatherModel
    {
        [JsonProperty("lastupdatedepoch")]
        public int LastUpdatedEpoch { get; set; }

        [JsonProperty("lastupdated")]
        public string LastUpdated { get; set; }

        [JsonProperty("tempc")]
        public double TempC { get; set; }

        [JsonProperty("tempf")]
        public double TempF { get; set; }

        [JsonProperty("isday")]
        public int IsDay { get; set; }

        [JsonProperty("condition")]
        public WeatherConditionModel Condition { get; set; }

        [JsonProperty("windmph")]
        public double WindMph { get; set; }

        [JsonProperty("windkph")]
        public double WindKph { get; set; }

        [JsonProperty("winddegree")]
        public int WindDegree { get; set; }

        [JsonProperty("winddir")]
        public string WindDir { get; set; }

        [JsonProperty("pressuremb")]
        public double PressureMb { get; set; }

        [JsonProperty("pressurein")]
        public double PressureIn { get; set; }

        [JsonProperty("precipmm")]
        public double PrecipMm { get; set; }

        [JsonProperty("precipin")]
        public double PrecipIn { get; set; }

        [JsonProperty("humidity")]
        public int Humidity { get; set; }

        [JsonProperty("cloud")]
        public int Cloud { get; set; }

        [JsonProperty("feelslikec")]
        public double FeelslikeC { get; set; }

        [JsonProperty("feelslikef")]
        public double FeelslikeF { get; set; }

        [JsonProperty("viskm")]
        public double VisKm { get; set; }

        [JsonProperty("vismiles")]
        public double VisMiles { get; set; }

        [JsonProperty("uv")]
        public double Uv { get; set; }

        [JsonProperty("gustmph")]
        public double GustMph { get; set; }

        [JsonProperty("gustkph")]
        public double GustKph { get; set; }
    }
}