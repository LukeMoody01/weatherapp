﻿using CoreWeatherAPI.Data;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CoreWeatherAPI.TokenAuthentication
{
    public interface ITokenManager
    {
        bool Authenticate(string email, string password);
        string NewToken(string emailAddress);
        ClaimsPrincipal VerifyToken(string token);
    }
}