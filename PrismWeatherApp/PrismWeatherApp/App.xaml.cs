using Prism;
using Prism.Ioc;
using Prism.Navigation;
using Prism.Plugin.Popups;
using PrismWeatherApp.ViewModels;
using PrismWeatherApp.Views;
using WeatherApp.Services;
using WeatherApp.Shared.Authentication;
using WeatherApp.Shared.BarrackShared;
using WeatherApp.Shared.GatewayAccess.Implementations;
using WeatherApp.Shared.GatewayAccess.Interfaces;
using WeatherApp.Shared.Repository;
using WeatherApp.Shared.Repository.Implementations;
using WeatherApp.Shared.Repository.Interfaces;
using WeatherApp.Shared.Services.Implementations;
using WeatherApp.Shared.Services.Interfaces;
using WeatherApp.Utility;
using Xamarin.Essentials.Implementation;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;

namespace PrismWeatherApp
{
    public partial class App
    {
        private readonly bool _flag;

        public App(IPlatformInitializer platformInitializer) : base(platformInitializer)
        {
        }

        public App(bool flag, IPlatformInitializer initializer = null)
            : base(initializer)
        {
            _flag = flag;
        }

        protected override void OnInitialized()
        {
            InitializeComponent();
        }

        protected override async void OnStart()
        {
            base.OnStart();
            var navP = new NavigationParameters();
            navP.Add("FromIntent", _flag);
            await NavigationService.NavigateAsync("AutoLoginPage", navP);
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<ILocalDBUtility, LocalDBUtility>();
            containerRegistry.RegisterSingleton<IAppInfo, AppInfoImplementation>();
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterPopupNavigationService();

            // SERVICES
            containerRegistry.RegisterSingleton<IAppUserService, AppUserService>();
            containerRegistry.RegisterSingleton<IAppSettingsProvider, AppSettingsProvider>();
            containerRegistry.RegisterSingleton<IWeatherService, WeatherService>();
            containerRegistry.RegisterSingleton<ISecureStorageHelper, SecureStorageHelper>();
            containerRegistry.RegisterSingleton<IAuthenticateManager, AuthenticateManager>();
            containerRegistry.RegisterSingleton<IAutoLoginService, AutoLoginService>();
            containerRegistry.RegisterSingleton<IAuthenticateService, AuthenticateService>();
            containerRegistry.RegisterSingleton<IPopupDialog, PopupDialog>();
            containerRegistry.Register<IBaseService, BaseService>();

            // REPOSITORIES
            containerRegistry.RegisterSingleton<IAppUserRepository, AppUserRepository>();
            containerRegistry.RegisterSingleton<IWeatherRepository, WeatherRepository>();

            containerRegistry.RegisterSingleton<IDBConnection>(sp =>
            {
                var conn = new DBConnection();
                var path = sp.Resolve<ILocalDBUtility>().GetDBPath(AppConstants.ENCRYPTED_DATABASE_NAME);
                conn.SetDBPath(path);
                return conn;
            });

            containerRegistry.RegisterSingleton<IDatabase>(sp =>
            {
                var localDBUtility = sp.Resolve<ILocalDBUtility>();
                var db = sp.Resolve<Database>();
                db.SetOldDBPath(localDBUtility.GetDBPath(AppConstants.DATABASE_NAME));
                db.DeleteOldDatabase();
                return db;
            });

            containerRegistry.RegisterSingleton<IStandardDBConnection>(sp =>
            {
                var conn = new StandardDBConnection();
                var path = sp.Resolve<ILocalDBUtility>().GetDBPath(AppConstants.STANDARD_DATABASE_NAME);
                conn.SetDBPath(path);
                conn.CreateDB();
                return conn;
            });

            // GATEWAYS
            containerRegistry.Register<IAppUserGateway, AppUserGateway>();
            containerRegistry.Register<IWeatherGateway, WeatherGateway>();

            // AUTH 
            containerRegistry.RegisterSingleton<ICurrentUser, CurrentUser>();

            // NAV
            containerRegistry.RegisterForNavigation<WeatherSearchPage, WeatherSearchPageViewModel>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<RegisterPage, RegisterPageViewModel>();
            containerRegistry.RegisterForNavigation<ForgottenPasswordPage, ForgottenPasswordPageViewModel>();
            containerRegistry.RegisterForNavigation<SelectLocationPage, SelectLocationPageViewModel>();
            containerRegistry.RegisterForNavigation<WeatherPage, WeatherPageViewModel>();
            containerRegistry.RegisterForNavigation<AutoLoginPage, AutoLoginPageViewModel>();
            containerRegistry.RegisterForNavigation<MainTabPage, MainTabPageViewModel>();
        }
    }
}
