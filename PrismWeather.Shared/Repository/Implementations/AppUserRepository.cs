﻿using SQLite;
using System;
using System.Threading.Tasks;
using WeatherApp.Shared.Models;
using WeatherApp.Shared.Repository.Interfaces;

namespace WeatherApp.Shared.Repository
{
    public class AppUserRepository : Repository<UserModel>, IAppUserRepository
    {
        public AppUserRepository(Lazy<IDBConnection> lazyDBConnection) : base(lazyDBConnection) { }

        // Temp to test out local db
        public async Task<int> SaveUser(UserModel user)
        {
            int result = -1;
            try
            {
                if (user != null)
                {
                    result = await InsertItem(user);
                }
            }
            catch (SQLiteException e)
            {
                throw e;
            }
            return result;
        }

        public async Task<UserModel> GetUser(string userId)
        {
            UserModel result = null;
            try
            {
                if (userId != null)
                {
                    result = await Get(x => x.Id == userId);
                }
            }
            catch (SQLiteException e)
            {
                throw e;
            }
            return result;
        }
    }
}
