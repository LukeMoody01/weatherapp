﻿using CoreWeatherAPI.Data;
using CoreWeatherAPI.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CoreWeatherAPI.TokenAuthentication
{
    public class TokenManager : ITokenManager
    {
        private DbUserModel user;
        private readonly IUserContextService _context;
        private readonly IConfiguration _configuration;
        private JwtSecurityTokenHandler tokenHandler;
        private byte[] secretKey; 

        public TokenManager(IUserContextService context,
            IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            tokenHandler = new JwtSecurityTokenHandler();
            string value = _configuration.GetValue<string>("SecretKey");
            secretKey = Convert.FromBase64String(value);
        }

        public bool Authenticate(string email, string password)
        {
            if (!string.IsNullOrWhiteSpace(email) &&
                !string.IsNullOrWhiteSpace(password))
            {
                user = _context.GetUser(email);
                if (user == null)
                    return false;
                else
                {
                    /* Fetch the stored value */
                    string savedPasswordHash = user.Password;
                    /* Extract the bytes */
                    byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
                    /* Get the salt */
                    byte[] salt = new byte[16];
                    Array.Copy(hashBytes, 0, salt, 0, 16);
                    /* Compute the hash on the password the user entered */
                    var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
                    byte[] hash = pbkdf2.GetBytes(20);
                    /* Compare the results */
                    for (int i = 0; i < 20; i++)
                        if (hashBytes[i + 16] != hash[i])
                            return false;
                    return true;
                }
            }
            else
                return false;
        }

        public string NewToken(string emailAddress)
        {
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] { new Claim(ClaimTypes.Email, emailAddress) }),
                Expires = DateTime.Now.AddYears(1), // Just a temp value to show it
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(secretKey),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var jwtString = tokenHandler.WriteToken(token);

            return jwtString;
        }

        public ClaimsPrincipal VerifyToken(string token)
        {
            var claims = tokenHandler.ValidateToken(token,
                new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(secretKey),
                    ValidateLifetime = true,
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);
            return claims;
        }
    }
}
